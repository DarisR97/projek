<?php

class ModelReport extends CI_Model {

    private $table = "trxanggaranh";


    var $column_order = array(null, 'idPermohonan');
    var $column_search = array('idPermohonan');
    var $order = array('idPermohonan' => 'DESC');
    
    public function get()
    {
        $this->db->get($this->table);
    }
    
	private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
        $this->db->join('refuser', 'refuser.id = trxanggaranh.UserId');

        $i = 0;
     
        foreach ($this->column_search as $item) // looping awal
        {
            if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
            {
                 
                if($i===0) // looping awal
                {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) 
                    $this->db->group_end(); 
            }
            $i++;
        }
        
        if ($this->session->userdata('id_role') == 1)
        {
            $this->db->where(['trxanggaranh.UserId' => $this->session->userdata('id_user')]);
        }
        
       if (isset($_POST['kegiatan']))
       {    
        if ($_POST['kegiatan'] != '')
        {
            $this->db->where(['trxanggaranh.CatatanKegiatan' => $_POST['kegiatan']]);
        }
       }

        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
	}
	
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function setujui_permohonan($array, $data)
    {
        $this->db->where($array);
        $this->db->update('trxanggaranh', $data);
    }


}