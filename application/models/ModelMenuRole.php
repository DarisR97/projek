<?php

class ModelMenuRole extends CI_Model {

    private $table = "role_menu_akses";

    public $id;
    public $menu_id;
    public $role_id;
    
    public function get()
    {
        return $this->db->get($this->table);
    }

    public function get_where($array)
    {
        $this->db->where($array);
        return $this->db->get($this->table);
    }

    public function add()
    {
        $data = $this->input->post();

        $this->menu         = $data['menu'];
        $this->created_at   = date('Y-m-d');

        $this->db->insert($this->table, $this);
    }

    public function edit()
    {
        $data = $this->input->post();

        $this->menu         = $data['menu'];
        $this->created_at   = date('Y-m-d');

        $this->db->where('id', $data['id']);
        $this->db->update($this->table, $this);
    }

    public function delete()
    {
        $this->db->where('id', $this->id);
        $this->db->delete($this->table);
    }


}