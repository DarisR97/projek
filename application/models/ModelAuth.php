<?php

class ModelAuth extends CI_Model {

    private $table = "refuser";

    public $id;
    public $nama;
    public $username;
    public $password;
    public $created_at;
    public $id_role;
    
    public function get()
    {
        return $this->db->get($this->table);
    }

    public function get_where($array)
    {
        $this->db->where($array);
        return $this->db->get($this->table);
    }

    public function check_login($username, $password)
    {
        $row = $this->db->get_where($this->table, ['username' => $username, 'password' => $password]);
        if ($row->num_rows() > 0) 
        {
            $data = array(
                'id_user' => $row->row(0)->id,
                'nama' => $row->row(0)->nama,
                'username' => $row->row(0)->username,
                'id_role' => $row->row(0)->id_role,
                'is_login' => true
            );

            $this->session->set_userdata($data);
            return true;
        }
        else
        {
            return false;
        }
    }

    public function rules()
    {
        $rules = array(
            [
                'field' => 'username',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => '<span class="text-danger">Tidak Boleh Kosong</span>',
                )
            ],
            [
                'field' => 'password',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => '<span class="text-danger">Tidak Boleh Kosong</span>',
                )
            ],
        );

        return $rules;
    }

}