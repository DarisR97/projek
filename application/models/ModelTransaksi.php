<?php

class ModelTransaksi extends CI_Model {

    private $table = "trxanggaranh";

    // public $IdPermohonan;
    // public $NoPermohonan;
    // public $TglPermohonan;
    // public $UserId;
    // public $CatatanKegiatan;
    // public $App1;
    // public $App2;
    // public $App3;
    // public $App4;
    // public $App5;
    // public $KetApp1;
    // public $KetApp2;
    // public $KetApp3;
    // public $KetApp4;
    // public $KetApp5;
    // public $Reject;
    // public $CatatanReject;
    // public $SysDate;



    var $column_order = array(null, 'idPermohonan');
    var $column_search = array('idPermohonan');
    var $order = array('idPermohonan' => 'DESC');
    
    public function get()
    {
        $this->db->get($this->table);
    }
    
    public function get_transaksi_detail($array)
    {
        $this->db->select('*');
        $this->db->from('trxanggarand');
        $this->db->where($array);
        $hasil = $this->db->get();
        return $hasil;
    }
    
    public function get_where($array)
    {
        $this->db->where($array);
        return $this->db->get($this->table);
    }

    public function get_where_join($array)
    {
        $this->db->join('refuser', 'refuser.id = trxanggaranh.UserId');
        $this->db->where($array);
        return $this->db->get($this->table);
    }

    public function add($table, $data)
    {
        $this->db->insert($table, $data);
    }

    public function edit()
    {
        $data = $this->input->post();

        $this->role         = $data['role'];
        $this->created_at   = date('Y-m-d');

        $this->db->where('id', $data['id']);
        $this->db->update($this->table, $this);
    }

    public function delete($id)
    {
        $this->db->where('id', $this->id);
        $this->db->delete($this->table);
    }

    public function rules_data()
    {
        $rules = array(
            [
                'field' => 'no_permohonan',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => '<span class="text-danger">Nama Tidak Boleh Kosong</span>',
                )
            ],
            [
                'field' => 'tgl_permohonan',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => '<span class="text-danger">Nama Tidak Boleh Kosong</span>',
                )
            ],
            [
                'field' => 'nama',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => '<span class="text-danger">Nama Tidak Boleh Kosong</span>',
                )
            ],
            [
                'field' => 'catatan_kegiatan',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => '<span class="text-danger">Nama Tidak Boleh Kosong</span>',
                )
            ]
        );

        return $rules;
    }

	private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
        $this->db->join('refuser', 'refuser.id = trxanggaranh.UserId');

        $i = 0;
     
        foreach ($this->column_search as $item) // looping awal
        {
            if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
            {
                 
                if($i===0) // looping awal
                {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) 
                    $this->db->group_end(); 
            }
            $i++;
        }
        
        if ($this->session->userdata('id_role') == 1)
        {
            $this->db->where(['trxanggaranh.UserId' => $this->session->userdata('id_user')]);
        }
		
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
	}
	
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function setujui_permohonan($array, $data)
    {
        $this->db->where($array);
        $this->db->update('trxanggaranh', $data);
    }

    public function get_count($array)
    {
        $this->db->where($array);
        return $this->db->get('trxanggaranh', $array);
    }


}