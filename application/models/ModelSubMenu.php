<?php

class ModelSubMenu extends CI_Model {

    private $table = "sub_menu";

    public $id;
    public $nama_sub;
    public $link;
    public $id_menu;

    var $column_order = array(null, 'id');
    var $column_search = array('nama_sub');
    var $order = array('id' => 'DESC');
    
    public function get()
    {
        return $this->db->get($this->table);
    }

    public function get_where($array)
    {
        $this->db->where($array);
        return $this->db->get($this->table);
    }

    public function add()
    {
        $data = $this->input->post();

        $this->nama_sub         = $data['nama_sub'];
        $this->link             = $data['link'];
        $this->id_menu          = $data['id_menu'];

        $this->db->insert($this->table, $this);
    }

    public function edit()
    {
        $data = $this->input->post();

        $this->id               = $data['id'];
        $this->nama_sub         = $data['nama_sub'];
        $this->link             = $data['link'];
        $this->id_menu          = $data['id_menu'];

        $this->db->where('id', $data['id']);
        $this->db->update($this->table, $this);
    }

    public function delete()
    {
        $this->db->where('id', $this->id);
        $this->db->delete($this->table);
    }

    public function rules()
    {
        $rules = array(
            [
                'field' => 'nama_sub',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => '<span class="text-danger">Nama Tidak Boleh Kosong</span>',
                )
            ],
            [
                'field' => 'link',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => '<span class="text-danger">Nama Tidak Boleh Kosong</span>',
                )
            ],
            [
                'field' => 'id_menu',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => '<span class="text-danger">Nama Tidak Boleh Kosong</span>',
                )
            ]

        );

        return $rules;
    }

	private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // looping awal
        {
            if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
            {
                 
                if($i===0) // looping awal
                {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) 
                    $this->db->group_end(); 
            }
            $i++;
		}
		
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
	}
	
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }


}