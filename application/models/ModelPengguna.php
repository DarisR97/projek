<?php

class ModelPengguna extends CI_Model {

    private $table = "refuser";

    public $id;
    public $nama;
    public $username;
    public $password;
    public $created_at;
    public $id_role;


    var $column_order = array(null, 'nama');
    var $column_search = array('nama');
    var $order = array('id' => 'DESC');
    
    public function get()
    {
        return $this->db->get($this->table);
    }

    public function get_where($array)
    {
        $this->db->where($array);
        return $this->db->get($this->table);
    }

    public function add()
    {
        $data = $this->input->post();

        $this->nama             = $data['nama'];
        $this->username         = $data['username'];
        $this->password         = md5($data['password']);
        $this->created_at       = date('Y-m-d');
        $this->id_role          = $data['id_role'];

        $this->db->insert($this->table, $this);
    }

    public function edit()
    {
        $data = $this->input->post();

        if ($data['password'] != null)
        {
            $array_data = array(
                'nama'      => $data['nama'],
                'username'  => $data['username'],
                'password'  => md5($data['password']),
                'created_at'=> date('Y-m-d'),
                'id_role'   => $data['id_role']
            );
        }
        else
        {
            $array_data = array(
                'nama'      => $data['nama'],
                'username'  => $data['username'],
                'created_at'=> date('Y-m-d'),
                'id_role'   => $data['id_role']
            );
        }
        
        $this->db->where('id', $data['id']);
        $this->db->update($this->table, $array_data);
    }

    public function delete()
    {
        $this->db->where('id', $this->id);
        $this->db->delete($this->table);
    }

    public function rules()
    {
        $rules = array(
            [
                'field' => 'nama',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => '<span class="text-danger">Tidak Boleh Kosong</span>',
                )
            ],
            [
                'field' => 'username',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => '<span class="text-danger">Tidak Boleh Kosong</span>',
                )
            ],
            [
                'field' => 'password',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => '<span class="text-danger">Tidak Boleh Kosong</span>',
                )
            ],
        );

        return $rules;
    }

    public function rules_ubah()
    {
        $rules = array(
            [
                'field' => 'nama',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => '<span class="text-danger">Tidak Boleh Kosong</span>',
                )
            ],
            [
                'field' => 'username',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => '<span class="text-danger">Tidak Boleh Kosong</span>',
                )
            ],
            [
                'field' => 'password',
                'rules' => 'trim',
                'errors' => array(
                    'required' => '<span class="text-danger">Tidak Boleh Kosong</span>',
                )
            ],
            [
                'field' => 'password_sekarang',
                'rules' => 'trim|required|callback_cek_password',
                'errors' => array(
                    'required' => '<span class="text-danger">Tidak Boleh Kosong</span>',
                    'cek_password' => '<span class="text-danger">Password Salah</span>',
                )
            ],
                
        );

        return $rules;
    }

	private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // looping awal
        {
            if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
            {
                 
                if($i===0) // looping awal
                {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) 
                    $this->db->group_end(); 
            }
            $i++;
		}
		
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
	}
	
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }


}