<!-- Isi Konten -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Laporan Transaksi </h1>
				</div><!-- /.col -->
				<div class="col-sm-6">

				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid"></div>

        <div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
                        <form id="cari">
                            <div class="row">
                                <div class="col-md-7">
                                    <input name="kegiatan" id="kegiatan" type="text" class="form-control form-control-sm" placeholder="kegiatan">
                                </div>
                                <div class="col-md-5">
                                    <div class="row">
                                        <div class="col">
                                            <button id="setujui" name="setujui" type="button" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Disetujui</button>
                                        </div>
                                        <div class="col">
                                            <button id="proses" name="proses" type="button" class="btn btn-primary btn-sm"><i class="fa fa-check"></i> Proses</button>
                                        </div>
                                        <div class="col">
                                            <button id="ditolak" name="ditolak" type="button" class="btn btn-danger btn-sm"><i class="fa fa-check"></i> Ditolak</button>
                                        </div>
                                        <div class="col">
                                            <button id="semua" name="semua" type="button" class="btn btn-secondary btn-sm"><i class="fa fa-check"></i> Semua</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
            
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<table id="pengajuan_table" class="table table-sm">
							<thead>
								<tr>
									<th>No</th>
									<th>Kegiatan</th>
									<th>Tanggal Pengajuan</th>
									<th>Nama</th>
									<th>Bagian</th>
									<th>Kode DPA</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>

							</body>
						</table>
					</div>
				</div>
			</div>
		</div>

</div>
</section>
</div>


<script src="<?php echo base_url('assets')?>/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url('assets')?>/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo base_url('assets')?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url('assets')?>/dist/js/adminlte.js"></script>
<script src="<?php echo base_url('assets')?>/plugins/moment/moment.min.js"></script>
<script src="<?php echo base_url('assets')?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url('assets')?>/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url('assets')?>/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url('assets')?>/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo base_url('assets')?>/dist/js/sweatalert.js"></script>
<script src="<?php echo base_url('assets')?>/dist/js/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets')?>/dist/js/tempusdominus-bootstrap-4.min.js"></script>

<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css"></script>

<script>

    var table

	$(document).ready(function() {
          
			table = $('#pengajuan_table').DataTable({
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
				"lengthMenu": [
					[10, 25, 50, -1],
					[10, 25, 50, "All"]
				],
				dom: 'lBfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
				"order": [],
				"ajax": {
					"url": "<?php echo site_url('report/ajax_table_report') ?>",
					"type": "POST",
				},
				"columnDefs": [{
					"targets": [0],
					"orderable": false,
					"searchable": false,
				}, ],
			});

             
        $("#setujui").on('click', function() {
            table = $('#pengajuan_table').DataTable({
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
				"lengthMenu": [
					[10, 25, 50, -1],
					[10, 25, 50, "All"]
				],
				dom: 'lBfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
				"order": [],
				"ajax": {
					"url": "<?php echo site_url('report/ajax_table_report_cari') ?>",
					"type": "POST",
                    "data": {
                        "aksi" : 1,
                        "kegiatan" : $('#kegiatan').val(),
                    },
				},
				"columnDefs": [{
					"targets": [0],
					"orderable": false,
					"searchable": false,
				}, ],
			});
        } )

        $("#proses").on('click', function() {
            table = $('#pengajuan_table').DataTable({
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
				"lengthMenu": [
					[10, 25, 50, -1],
					[10, 25, 50, "All"]
				],
				dom: 'lBfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
				"order": [],
				"ajax": {
					"url": "<?php echo site_url('report/ajax_table_report_cari') ?>",
					"type": "POST",
                    "data": {
                        "aksi" : 2,
                        "kegiatan" : $('#kegiatan').val(),
                    },
				},
				"columnDefs": [{
					"targets": [0],
					"orderable": false,
					"searchable": false,
				}, ],
			});
        } )

        $("#ditolak").on('click', function() {
            table = $('#pengajuan_table').DataTable({
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
				"lengthMenu": [
					[10, 25, 50, -1],
					[10, 25, 50, "All"]
				],
				dom: 'lBfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
				"order": [],
				"ajax": {
					"url": "<?php echo site_url('report/ajax_table_report_cari') ?>",
					"type": "POST",
                    "data": {
                        "aksi" : 3,
                        "kegiatan" : $('#kegiatan').val(),
                    },
				},
				"columnDefs": [{
					"targets": [0],
					"orderable": false,
					"searchable": false,
				}, ],
			});
        } )

        $("#semua").on('click', function() {
            table = $('#pengajuan_table').DataTable({
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
				"lengthMenu": [
					[10, 25, 50, -1],
					[10, 25, 50, "All"]
				],
				dom: 'lBfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
				"order": [],
				"ajax": {
					"url": "<?php echo site_url('report/ajax_table_report_cari') ?>",
					"type": "POST",
                    "data": {
                        "aksi" : 4,
                        "kegiatan" : $('#kegiatan').val(),
                    },
				},
				"columnDefs": [{
					"targets": [0],
					"orderable": false,
					"searchable": false,
				}, ],
			});
        } )
	});
</script>


</body>

</html>
<!-- ./wrapper -->