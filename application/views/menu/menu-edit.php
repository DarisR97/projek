
  <!-- Isi Konten -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Menu</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Menu</h3>
                    </div>

                    <div class="card-body">
                        <?php if ($this->session->flashdata('berhasil')) echo $this->session->flashdata('berhasil')  ?>
                        <form method="post" action="<?php echo site_url('menu/submit_edit')?>">
                            <input value="<?php echo $menu->id?>" type="hidden" class="form-control form-control-sm" id="id" name="id" placeholder="Masukan nama role">
                            <div class="form-group">
                                <label>Nama Menu</label>
                                <input value="<?php echo $menu->menu?>" type="text" class="form-control form-control-sm" id="menu" name="menu" placeholder="Masukan nama menu">
                                <small id="help" class="form-text text-muted"><?php echo form_error('menu'); ?></small>
                            </div>
                            <div class="form-group">
                                <label>Link</label>
                                <input value="<?php echo $menu->link ?>" type="text" class="form-control form-control-sm" id="link" name="link" placeholder="Masukan link">
                                <small id="help" class="form-text text-muted"><?php echo form_error('link'); ?></small>
                            </div>
                            <div class="form-group">
                                <label>Icon</label>
                                <input value="<?php echo $menu->icon?>" type="text" class="form-control form-control-sm" id="icon" name="icon" placeholder="Masukan icon">
                                <small id="help" class="form-text text-muted"><?php echo form_error('icon'); ?></small>
                            </div>
                            <button type="submit" name="tambah" class="btn btn-primary btn-sm">Simpan Data</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </section>
</div>

<?php $this->load->view('footer'); ?>




</body>
</html>
<!-- ./wrapper -->

