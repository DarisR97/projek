
  <!-- Isi Konten -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Menu</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
          
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                    </div>

                    <div class="card-body">
                    <div class="row">
                      <div class="col-md-5">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Pilih Role</label>
    
                        <select id="role" class="custom-select">
                        <?php
                            foreach($role->result() as $field) {
                        ?>
                            <option value="<?php echo $field->id?>"><?php echo $field->role?></option>
                        <?php 
                            } 
                        ?>
                        </select>
                      </div>

                      </div>
                      <div class="col-md-1">
                     
                      </div>
                      <div class="col-md-6">
                        <!--  -->
                        <div id="table_r">
                        
                        </div>
                        <!--  -->
                      </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </section>
</div>

<?php $this->load->view('footer'); ?>

<script type="text/javascript">
    $(document).ready(function(){
    });
</script>

<script type="text/javascript">
    // function loadData digunakan untuk menampilkan table yang ada di function module
    function addRule(id_modul)
    {
        $.ajax({
            type    : 'POST',
            url     : '<?php echo base_url() ?>menu/add_rule',
            data    : {
                role: $('#role').val(),
                id_menu: id_modul,
            },
            success : function(html) {
                alert("Sukses Merubah Hak Akses");
            }
        })
        
    }

    $('#role').on('change', function() {
        $.ajax({
            type    : 'POST',
            url     : '<?php echo base_url() ?>menu/get_rule_menu',
            data    : {
                role: $(this).val(),
            },
            success : function(html) {
                $("#table_r").html(html);
            }
        })
    })


</script>
</body>
</html>
<!-- ./wrapper -->

