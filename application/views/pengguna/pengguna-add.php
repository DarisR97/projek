
  <!-- Isi Konten -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Pengguna</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
           
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Pengguna</h3>
                    </div>

                    <div class="card-body">
                        <?php if ($this->session->flashdata('berhasil')) echo $this->session->flashdata('berhasil')  ?>
                        <form method="post" action="<?php echo site_url('pengguna/submit')?>">
                            <div class="form-group">
                                <label>Nama</label>
                                <input value="<?php echo set_value('nama'); ?>" type="text" class="form-control form-control-sm" id="nama" name="nama" placeholder="Masukan nama pengguna">
                                <small id="help" class="form-text text-muted"><?php echo form_error('nama'); ?></small>
                            </div>
                            <div class="form-group">
                                <label>Username</label>
                                <input value="<?php echo set_value('username'); ?>" type="text" class="form-control form-control-sm" id="username" name="username" placeholder="Masukan username">
                                <small id="help" class="form-text text-muted"><?php echo form_error('username'); ?></small>
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input value="<?php echo set_value('password'); ?>" type="password" class="form-control form-control-sm" id="password" name="password" placeholder="Masukan password pengguna">
                                <small id="help" class="form-text text-muted"><?php echo form_error('password'); ?></small>
                            </div>
                            <div class="form-group">
                                <label>Role / Bagian</label>
                                <select id="id_role" name="id_role" class="custom-select">
                                <?php
                                    foreach($role->result() as $field) {
                                ?>
                                
                                    <option value="<?php echo $field->id?>"><?php echo $field->role?></option>
                               
                                <?php 
                                    }
                                ?>
                                </select>

                                <small id="help" class="form-text text-muted"><?php echo form_error('role'); ?></small>
                            </div>
                            <button type="submit" name="tambah" class="btn btn-primary btn-sm">Simpan Data</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </section>
</div>

<?php $this->load->view('footer'); ?>




</body>
</html>
<!-- ./wrapper -->

