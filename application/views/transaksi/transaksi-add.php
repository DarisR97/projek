
  <!-- Isi Konten -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Ajukan Permohonan</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
          
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <!-- data -->
        <div id="data">
        <div class="row">
            <div class="col-12">
                <div class="card">
                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item active" aria-current="page">Isi form untuk pengajuan</li>
                        </ol>
                      </nav>

                    <div class="card-body">
                    <?php if ($this->session->set_flashdata('message')) echo $this->session->set_flashdata('success') ?>
                    <form class="needs-validation" id="set_data" novalidate>
                      <div class="form-row">
                        <div class="col-md-3 mb-3">
                          <label for="validationCustom01">No Permohonan</label>
                          <input name="no_permohonan" placeholder="No Permohonan" type="text" class="form-control" id="validationCustom01" placeholder="First name" required>
                          <div class="invalid-feedback">
                            Isi nomor permohonan
                          </div>
                        </div>
                        <div class="col-md-3 mb-3">
                          <label for="validationCustom02">Tgl Permohonan</label>
                            <!--  -->
                              <!-- <div class="form-group">
                                <div class="input-group date" id="datetimepicker11" data-target-input="nearest">
                                    <input  name="tgl_permohonan" placeholder="DD - MM - YYYY" name="tgl_permohonan" id="datetimepicker11" type="text" class="form-control datetimepicker-input" data-target="#datetimepicker11"/>
                                    <div class="input-group-append" data-target="#datetimepicker11" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                              </div> -->
                            <!--  -->
                            <div class="form-group">
                                <div class="input-group date" id="datetimepicker4" data-target-input="nearest">
                                    <input required name="tgl_permohonan" placeholder="DD - MM - YYYY" name="tgl_permohonan" type="text" class="form-control datetimepicker-input" data-target="#datetimepicker4"/>
                                    <div class="input-group-append" data-target="#datetimepicker4" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                          <label for="validationCustomUsername">Nama</label>
                          <div class="input-group">
                            <input value="<?php echo $this->session->userdata('id_user')?>" name="nama" type="hidden" class="form-control" id="validationCustomUsername" placeholder="Nama" aria-describedby="inputGroupPrepend" required>
                            <input disabled value="<?php echo $this->session->userdata('nama')?>" type="text" class="form-control" id="validationCustomUsername" placeholder="Nama" aria-describedby="inputGroupPrepend" required>
                            <div class="invalid-feedback">
                              Isi nama
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3 mb-3">
                          <label for="validationCustom03">Catatan Kegiatan</label>
                          <input name="catatan_kegiatan" type="text" class="form-control" id="validationCustom03" placeholder="Catatan Kegiatan" required>
                          <div class="invalid-feedback">
                            Isi catatan kegiatan
                          </div>
                        </div>
                      </div>
                      
                     
                      <button style="float: right;" class="btn btn-primary" type="submit">Ajukan Permohonan <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button>
                    </form>

                        <?php if ($this->session->flashdata('berhasil')) echo $this->session->flashdata('berhasil')  ?>
                        
                    </div>
                </div>
            </div>
        </div>

        </div>
        <!-- end data -->

        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-body">
                        <table id="pengajuan_table" class="table table-sm">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Catatan Kegiatan</th>
                                    <th>Tanggal Pengajuan</th>
                                    <th>Nama</th>
                                    <th>Status</th>
                                    <th style="width: 28%;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                             
                            </body>
                        </table>
                    </div>
                </div>
            </div>
        </div>

      </div>
    </section>
</div>

<div id="modal_hapus" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Hapus Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="modal-body" class="modal-body">
        <p>Yakin ingin menghapus data ini ?</p>
      </div>
      <div class="modal-footer">
        <form id="hapus_form" method="post" action="<?php echo site_url('transaksi/hapus/')?>">
          <input type="hidden" name="pengajuan_id" id="pengajuan_id">
          <button type="submit" class="btn btn-primary">Hapus</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        </form>
      </div>
    </div>
  </div>
</div>


<?php $this->load->view('footer'); ?>
<script type="text/javascript" src="<?php echo base_url('assets')?>/dist/js/webcam.min.js"></script>

<script>
    var table

$(document).ready(function(){
      $('#datetimepicker4').datetimepicker({
        format: 'DD-MM-YYYY'
      });
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        } else {
          event.preventDefault();
          // mulai
            $.ajax({
              url       : '<?php echo site_url('transaksi/set_data') ?>',
              data      : $('#set_data').serialize(),
              type      :'POST',
              dataType  : 'JSON',
              beforeSend: function() {
                // $("textarea").attr("disabled",true);
                // $("button").attr("disabled",true);
              },
              complete:function() {
                // $("textarea").attr("disabled",false);
                // $("button").attr("disabled",false);								
              },
              success:function(hasil) {
                // if (hasil.status == 500)
                // {
                //   console.log(hasil)
                // } 
                // else if (hasil.status == 200)
                // {
                //   window.location.href = "<?php echo site_url('transaksi/lengkapi_berkas/')?>" + hasil.data;
                // }
                $('#pengajuan_table').DataTable().ajax.reload();
                Swal.fire({
                  title: 'Berhasil, silahkan lengkapi berkas anda',
                  showConfirmButton: false,
                  timer: 2000
                })
                $("#set_data").trigger("reset");
              }
            })
          
        }
        form.classList.add('was-validated');
      }, false);
    });


    $('#ambil_gambar1').on('click', function() {
      take_snapshot1()
    })

    $('#ambil_gambar2').on('click', function() {
      take_snapshot2()
    })

    $('#ambil_gambar3').on('click', function() {
      take_snapshot3()
    })

    $('#ambil_gambar4').on('click', function() {
      take_snapshot4()
    })

    $('#ambil_gambar5').on('click', function() {
      take_snapshot5()
    })

    function take_snapshot1() {
    
        // take snapshot and get image data
        Webcam.snap(function (data_uri) {                    
            document.getElementById('results1').innerHTML =
                            '<img class="img-fluid" src="' + data_uri + '"/>';
            $('#foto1').val(data_uri);
            Webcam.upload( data_uri, '<?php echo site_url('transaksi/upload_foto/' . 1); ?>', function(code, text) {
                if (code == '200') {
                    $('#modal_foto1').modal('hide');
                } else {
                    alert('error');
                }
            } );
        });
    }

    function take_snapshot2() {
    
      // take snapshot and get image data
      Webcam.snap(function (data_uri) {                    
          document.getElementById('results2').innerHTML =
                          '<img class="img-fluid" src="' + data_uri + '"/>';
          $('#foto2').val(data_uri);
          Webcam.upload( data_uri, '<?php echo site_url('transaksi/upload_foto/' . 2); ?>', function(code, text) {
              if (code == '200') {
                  $('#modal_foto2').modal('hide');
              } else {
                  alert('error');
              }
          } );
      });
    }

    function take_snapshot3() {
    
    // take snapshot and get image data
    Webcam.snap(function (data_uri) {                    
        document.getElementById('results3').innerHTML =
                        '<img class="img-fluid" src="' + data_uri + '"/>';
        $('#foto3').val(data_uri);
        Webcam.upload( data_uri, '<?php echo site_url('transaksi/upload_foto/' . 3); ?>', function(code, text) {
            if (code == '200') {
                $('#modal_foto3').modal('hide');
            } else {
                alert('error');
            }
        } );
    });
  }

  function take_snapshot4() {
    
    // take snapshot and get image data
    Webcam.snap(function (data_uri) {                    
        document.getElementById('results4').innerHTML =
                        '<img class="img-fluid" src="' + data_uri + '"/>';
        $('#foto4').val(data_uri);
        Webcam.upload( data_uri, '<?php echo site_url('transaksi/upload_foto/' . 4); ?>', function(code, text) {
            if (code == '200') {
                $('#modal_foto4').modal('hide');
            } else {
                alert('error');
            }
        } );
    });
  }

  function take_snapshot5() {
    
    // take snapshot and get image data
    Webcam.snap(function (data_uri) {                    
        document.getElementById('results5').innerHTML =
                        '<img class="img-fluid" src="' + data_uri + '"/>';
        $('#foto5').val(data_uri);
        Webcam.upload( data_uri, '<?php echo site_url('transaksi/upload_foto/' . 5); ?>', function(code, text) {
            if (code == '200') {
                $('#modal_foto5').modal('hide');
            } else {
                alert('error');
            }
        } );
    });
  }

    $('#popup_foto1').on('click', function() {
        $('#modal_foto1').modal('show');

         //foto
          Webcam.set({
              width: 520,
              height: 440,
              image_format: 'jpeg',
              jpeg_quality: 100
          });

          Webcam.attach( '#my_camera1' );
    })

    $('#popup_foto2').on('click', function() {
        $('#modal_foto2').modal('show');

           //foto
           Webcam.set({
              width: 520,
              height: 440,
              image_format: 'jpeg',
              jpeg_quality: 100
          });

          Webcam.attach( '#my_camera2' );
    })

    $('#popup_foto3').on('click', function() {
        $('#modal_foto3').modal('show');

           //foto
           Webcam.set({
              width: 520,
              height: 440,
              image_format: 'jpeg',
              jpeg_quality: 100
          });

          Webcam.attach( '#my_camera3' );
    })

    $('#popup_foto4').on('click', function() {
        $('#modal_foto4').modal('show');

           //foto
           Webcam.set({
              width: 520,
              height: 440,
              image_format: 'jpeg',
              jpeg_quality: 100
          });

          Webcam.attach( '#my_camera4' );
    })

    $('#popup_foto5').on('click', function() {
        $('#modal_foto5').modal('show');

           //foto
           Webcam.set({
              width: 520,
              height: 440,
              image_format: 'jpeg',
              jpeg_quality: 100
          });

          Webcam.attach( '#my_camera5' );
    })
  
    $(document).ready(function(){
        table = $('#pengajuan_table').DataTable({
          "bDestroy": true,
          "processing": true, 
          "serverSide": true,
          "order": [], 
                
          "ajax": {
            "url": "<?php echo site_url('transaksi/ajax_table_transaksi_pengajuan')?>",
            "type": "POST",
          },
      
          "columnDefs": [
            { 
              "targets": [ 0 ], 
              "orderable": false,
              "searchable": false,
            },
          ],
        });

      $('#pengajuan_table').on('click', '#hapus', function() {
        var id = $(this).attr('data-id');
        $('#modal_hapus').modal('show');
        $('#pengajuan_id').val(id);
      })
            
    });

      $('#hapus_form').submit( function() {
        // 
            event.preventDefault();
            // mulai
            $.ajax({
              url       : $(this).attr('action'),
              data      : $('#hapus_form').serialize(),
              type      :'POST',
              dataType  : 'JSON',
              beforeSend: function() {
                // $("textarea").attr("disabled",true);
                // $("button").attr("disabled",true);
              },
              complete:function() {
                // $("textarea").attr("disabled",false);
                // $("button").attr("disabled",false);								
              },
              success:function(hasil) {
                $('#modal_hapus').modal('hide');
                $('#pengajuan_table').DataTable().ajax.reload();
              }
            })
        // 
      })
});

</script>


</body>
</html>
<!-- ./wrapper -->

