
  <!-- Isi Konten -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Lihat Hasil Persetujuan</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
      
        <!-- data -->
        <div id="data">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                      <!-- <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="#">Isi Data</a></li>
                          <li class="breadcrumb-item active" aria-current="page">Tambah Foto</li>
                        </ol>
                      </nav> -->
                    </div>
                    <?php
                      
                        $status = 'onclick="return false"';
                        // $status_berkas2 = '';
                        // $status_tangan2  = '';
                        // $status_rencana2 = '';
                        // $status_norek2   = '';

                        // $status_berkas1 = '';
                        // $status_tangan1  = '';
                        // $status_rencana1 = '';
                        // $status_norek1   = '';

                        // if ($dana->berkas == 1)
                        // {
                        //     $status_berkas1 = 'checked';
                        // }
                        // else
                        // {
                        //     $status_berkas2 = 'checked';
                        // }

                        // if ($dana->tanda_tangan == 1)
                        // {
                        //     $status_tangan1 = 'checked';
                        // }
                        // else
                        // {
                        //     $status_tangan2 = 'checked';
                        // }

                        // if ($dana->rencana_pelaksanaan == 1)
                        // {
                        //     $status_rencana1 = 'checked';
                        // }
                        // else
                        // {
                        //     $status_rencana2 = 'checked';
                        // }

                        // if ($dana->no_rekening == 1)
                        // {
                        //     $status_norek1 = 'checked';
                        // }
                        // else
                        // {
                        //     $status_norek2 = 'checked';
                        // }
                    ?>
                    <input type="hidden" id="id" value="<?php echo $h->IdPermohonan?>">
                    <div class="card-body">
                    <div class="row">
                      <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box">
                          <div class="info-box-content">
                            <span class="info-box-text">No Permohonan</span>
                            <span class="info-box-number">
                            <?php echo $h->NoPermohonan ?>
                            </span>
                          </div>
                          <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                      </div>
                      <!-- /.col -->
                      <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box mb-3">
                          <div class="info-box-content">
                            <span class="info-box-text">Tgl Permohonan</span>
                            <span class="info-box-number"><?php echo $h->TglPermohonan?></span>
                          </div>
                          <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                      </div>
                      <!-- /.col -->

                      <!-- fix for small devices only -->
                      <div class="clearfix hidden-md-up"></div>

                      <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box mb-3">

                          <div class="info-box-content">
                            <span class="info-box-text">Nama</span>
                            <span class="info-box-number"><?php echo $h->UserId ?></span>
                          </div>
                          <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                      </div>
                      <!-- /.col -->
                      <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box mb-3">

                          <div class="info-box-content">
                            <span class="info-box-text">Catatan Kegiatan</span>
                            <span class="info-box-number"><?php echo $h->CatatanKegiatan ?></span>
                          </div>
                          <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                      </div>
                      <!-- /.col -->
                    </div>

                    <div class="row">
                        <div class="col-md-1">
                        </div>
                        <div class="col-md-2">
                           <div class="card">
                                <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-check form-check-inline">
                                                    <span class="btn btn-sm btn-outline-secondary btn-block">
                                                    <?php 
                                                        if ($h->App1 == 1) {
                                                    ?>
                                                        <input <?php echo $status; ?> checked class="form-check-input" type="checkbox" id="check1" value="option1">
                                                    <?php 
                                                        } else {
                                                    ?>
                                                        <input <?php echo $status; ?> class="form-check-input" type="checkbox" id="check1" value="option1">
                                                    <?php } ?>
                                                    <label class="form-check-label" for="inlineCheckbox1">Setujui</label>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <button id="lihat_1" class="btn btn-sm btn-success"><i class="fa fa-camera-retro"></i> PREVIEW</button>
                                            </div>
                                        </div>
                                </div>
                           </div>
                        </div>
                        <div class="col-md-2">
                            <div class="card">
                                <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-check form-check-inline">
                                                    <span class="btn btn-sm btn-outline-secondary btn-block">
                                                    <?php 
                                                        if ($h->App2 == 1) {
                                                    ?>
                                                        <input <?php echo $status; ?> checked class="form-check-input" type="checkbox" id="check2" value="option1">
                                                    <?php 
                                                        } else {
                                                    ?>
                                                        <input <?php echo $status; ?> class="form-check-input" type="checkbox" id="check2" value="option1">
                                                    <?php } ?>
                                                    <label class="form-check-label" for="inlineCheckbox1">Setujui</label>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <button id="lihat_2" class="btn btn-sm btn-success"><i class="fa fa-camera-retro"></i> PREVIEW</button>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="card">
                                <div class="card-body">
                                <!--  -->
                                <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-check form-check-inline">
                                                    <span class="btn btn-sm btn-outline-secondary btn-block">
                                                    <?php 
                                                        if ($h->App3 == 1) {
                                                    ?>
                                                        <input <?php echo $status; ?> checked class="form-check-input" type="checkbox" id="check3" value="option1">
                                                    <?php 
                                                        } else {
                                                    ?>
                                                        <input <?php echo $status; ?> class="form-check-input" type="checkbox" id="check3" value="option1">
                                                    <?php } ?>
                                                    <label class="form-check-label" for="inlineCheckbox1">Setujui</label>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <button id="lihat_3" class="btn btn-sm btn-success"><i class="fa fa-camera-retro"></i> PREVIEW</button>
                                            </div>
                                        </div>
                                <!--  -->
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="card">
                                <div class="card-body">
                                <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-check form-check-inline">
                                                    <span class="btn btn-sm btn-outline-secondary btn-block">
                                                    <?php 
                                                        if ($h->App4 == 1) {
                                                    ?>
                                                        <input <?php echo $status; ?> checked class="form-check-input" type="checkbox" id="check4" value="option1">
                                                    <?php 
                                                        } else {
                                                    ?>
                                                        <input <?php echo $status; ?> class="form-check-input" type="checkbox" id="check4" value="option1">
                                                    <?php } ?>
                                                    <label class="form-check-label" for="inlineCheckbox1">Setujui</label>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <button id="lihat_4" class="btn btn-sm btn-success"><i class="fa fa-camera-retro"></i> PREVIEW</button>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="card">
                                <div class="card-body">
                                <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-check form-check-inline">
                                                    <span class="btn btn-sm btn-outline-secondary btn-block">
                                                    <?php 
                                                        if ($h->App5 == 1) {
                                                    ?>
                                                        <input <?php echo $status; ?> checked class="form-check-input" type="checkbox" id="check5" value="option1">
                                                    <?php 
                                                        } else {
                                                    ?>
                                                        <input <?php echo $status; ?> class="form-check-input" type="checkbox" id="check5" value="option1">
                                                    <?php } ?>
                                                    <label class="form-check-label" for="inlineCheckbox1">Setujui</label>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <button id="lihat_5" class="btn btn-sm btn-success"><i class="fa fa-camera-retro"></i> PREVIEW</button>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1">
                        </div>
                    </div>


                    <!-- <div class="row">
                        <div class="col-md-12">
                         
                            <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">APP 1</th>
                                    <th scope="col">APP 2</th>
                                    <th scope="col">APP 3</th>
                                    <th scope="col">APP 4</th>
                                    <th scope="col">APP 5</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-check form-check-inline">
                                                    <span class="btn btn-sm btn-outline-secondary btn-block">
                                                    <?php 
                                                        if ($h->App1 == 1) {
                                                    ?>
                                                        <input <?php echo $status; ?> checked class="form-check-input" type="checkbox" id="check1" value="option1">
                                                    <?php 
                                                        } else {
                                                    ?>
                                                        <input <?php echo $status; ?> class="form-check-input" type="checkbox" id="check1" value="option1">
                                                    <?php } ?>
                                                    <label class="form-check-label" for="inlineCheckbox1">Setujui</label>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <button id="lihat_1" class="btn btn-sm btn-success"><i class="fa fa-camera-retro"></i> PREVIEW</button>
                                            </div>
                                        </div>
                                    </th>
                                    <th scope="row">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-check form-check-inline">
                                                    <span class="btn btn-sm btn-outline-secondary btn-block">
                                                    <?php 
                                                        if ($h->App2 == 1) {
                                                    ?>
                                                        <input <?php echo $status; ?> checked class="form-check-input" type="checkbox" id="check2" value="option1">
                                                    <?php 
                                                        } else {
                                                    ?>
                                                        <input <?php echo $status; ?> class="form-check-input" type="checkbox" id="check2" value="option1">
                                                    <?php } ?>
                                                    <label class="form-check-label" for="inlineCheckbox1">Setujui</label>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <button id="lihat_2" class="btn btn-sm btn-success"><i class="fa fa-camera-retro"></i> PREVIEW</button>
                                            </div>
                                        </div>
                                    </th>
                                    <th scope="row">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-check form-check-inline">
                                                    <span class="btn btn-sm btn-outline-secondary btn-block">
                                                    <?php 
                                                        if ($h->App3 == 1) {
                                                    ?>
                                                        <input <?php echo $status; ?> checked class="form-check-input" type="checkbox" id="check3" value="option1">
                                                    <?php 
                                                        } else {
                                                    ?>
                                                        <input <?php echo $status; ?> class="form-check-input" type="checkbox" id="check3" value="option1">
                                                    <?php } ?>
                                                    <label class="form-check-label" for="inlineCheckbox1">Setujui</label>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <button id="lihat_3" class="btn btn-sm btn-success"><i class="fa fa-camera-retro"></i> PREVIEW</button>
                                            </div>
                                        </div>
                                    </th>
                                    <th scope="row">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-check form-check-inline">
                                                    <span class="btn btn-sm btn-outline-secondary btn-block">
                                                    <?php 
                                                        if ($h->App4 == 1) {
                                                    ?>
                                                        <input <?php echo $status; ?> checked class="form-check-input" type="checkbox" id="check4" value="option1">
                                                    <?php 
                                                        } else {
                                                    ?>
                                                        <input <?php echo $status; ?> class="form-check-input" type="checkbox" id="check4" value="option1">
                                                    <?php } ?>
                                                    <label class="form-check-label" for="inlineCheckbox1">Setujui</label>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <button id="lihat_4" class="btn btn-sm btn-success"><i class="fa fa-camera-retro"></i> PREVIEW</button>
                                            </div>
                                        </div>
                                    </th>
                                    <th scope="row">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-check form-check-inline">
                                                    <span class="btn btn-sm btn-outline-secondary btn-block">
                                                    <?php 
                                                        if ($h->App5 == 1) {
                                                    ?>
                                                        <input <?php echo $status; ?> checked class="form-check-input" type="checkbox" id="check5" value="option1">
                                                    <?php 
                                                        } else {
                                                    ?>
                                                        <input <?php echo $status; ?> class="form-check-input" type="checkbox" id="check5" value="option1">
                                                    <?php } ?>
                                                    <label class="form-check-label" for="inlineCheckbox1">Setujui</label>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <button id="lihat_5" class="btn btn-sm btn-success"><i class="fa fa-camera-retro"></i> PREVIEW</button>
                                            </div>
                                        </div>
                                    </th>
                                   
                                </tr>
                            </tbody>
                            </table>
                       
                        </div> 
                    </div> -->
                    
                    <div class="row">
                        <div class="col-md-12">
                            <!--  -->
                            <div class="input-group">
                                <input value="<?php echo $h->CatatanReject?>" name="catatan" id="catatan" type="text" class="form-control form-control-sm" placeholder="Catatan Reject" required>
                            </div>
                            <!--  -->
                        </div>
                        <!-- <div class="col-md-2">
                            <button style="float: right;" id="reject" class="btn btn-sm btn-danger btn-block"><i class="fa fa-times"></i> REJECT</button>
                            <button style="float: right;" id="cancel" class="btn btn-sm btn-success btn-block"><i class="fa fa-check"></i> CANCEL REJECT</button>
                        </div> -->
                    </div>

                        <?php if ($this->session->flashdata('berhasil')) echo $this->session->flashdata('berhasil')  ?>
                        
                    </div>
                </div>
            </div>
        </div>

        </div>
        <!-- end data -->
        

      
        <div class="row">
            
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                       <label>FILE</label>
                       <table class="table table-sm">
                            <tr>
                                <td>Nama File</td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td><?php 
                                    if ($h->berkas_dana != '') {
                                        echo $h->berkas_dana;
                                    } else {
                                        echo 'Tidak ada file';
                                    }
                                ?></td>
                                <td>
                                    <a href="<?php echo site_url('dana/download_file/' . $h->IdPermohonan)?>"  class="btn btn-sm btn-primary"><i class="fa fa-download"></i> Download Berkas</a>
                                </td>
                            </tr>
                       </table>
                    </div>
                </div>
            </div>
        </div>


      </div>
    </section>
</div>
<?php 
    $arr = array('1', '2', '3', '4', '5');
    foreach($d as $field) {
   
?>
<div id="modal_foto<?php echo $field->App?>" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!--  -->
      <img src="<?php echo base_url('assets/') . $field->Dokumen?>" class="img-fluid" alt="Responsive image">
      <!--  -->
    </div>
  </div>
</div>
<?php } ?>



<?php $this->load->view('footer'); ?>
<script type="text/javascript" src="<?php echo base_url('assets')?>/dist/js/webcam.min.js"></script>

<script>
$(document).ready(function(){
    $('#lihat_1').on('click', function() {
        $('#modal_foto1').modal('show');
    })

    $('#lihat_2').on('click', function() {
        $('#modal_foto2').modal('show');
    })

    $('#lihat_3').on('click', function() {
        $('#modal_foto3').modal('show');
    })

    $('#lihat_4').on('click', function() {
        $('#modal_foto4').modal('show');
    })

    $('#lihat_5').on('click', function() {
        $('#modal_foto5').modal('show');
    })

});

</script>


</body>
</html>
<!-- ./wrapper -->

