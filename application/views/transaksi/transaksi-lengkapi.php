
  <!-- Isi Konten -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Lengkapi Berkas</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
          
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <!-- foto -->
         <!-- data -->
         <div id="foto">
        <div class="row">
            <div class="col-12">
                <div class="card">
                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item active" aria-current="page">lengkapi berkas</li>
                        </ol>
                      </nav>

                    <div class="card-body">
                          
<!--                           
                    <div class='row'>
                      <div class="col-12">
                      <div class="input-group">
                            <input type="submit" class="btn btn-sm btn-primary btn-block" value="simpan transaksi">
                          </div>
                      </div>
                    </div> -->
                    
                    <div class="row">
                     
                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box bg-light">
                          <div class="info-box-content">
                            <span class="info-box-text text-center text-muted">No Permohonan</span>
                            <span class="info-box-number text-center text-muted mb-0"><?php echo $h->NoPermohonan ?></span>
                          </div>
                        </div>
                      </div>
                   
                      <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box bg-light">
                          <div class="info-box-content">
                            <span class="info-box-text text-center text-muted">Tanggal Pengajuan</span>
                            <span class="info-box-number text-center text-muted mb-0"><?php echo $h->TglPermohonan ?></span>
                          </div>
                        </div>
                      </div>

                      <!-- fix for small devices only -->
                      <div class="clearfix hidden-md-up"></div>

                      <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box bg-light">
                          <div class="info-box-content">
                            <span class="info-box-text text-center text-muted">Nama</span>
                            <span class="info-box-number text-center text-muted mb-0"><?php echo $h->UserId ?></span>
                          </div>
                        </div>
                      </div>
                      <!-- /.col -->
                     
                      <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box bg-light">
                          <div class="info-box-content">
                            <span class="info-box-text text-center text-muted">Catatan Kegiatan</span>
                            <span class="info-box-number text-center text-muted mb-0"><?php echo $h->CatatanKegiatan ?></span>
                          </div>
                        </div>
                      </div>

                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <div class="card">
                            <div id="results1" ></div>
                            <input type="hidden" name="foto1" id="foto1">
                            <div class="card-body">
                                <form id="form1" action="<?php echo site_url('transaksi/upload_foto_2'); ?>" method="post" enctype="multipart/form-data">
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input name="file" type="file" accept="image/*" capture="camera" class="custom-file-input" id="upload_foto1">
                                            <label class="custom-file-label" for="inputGroupFile04">Pilih Foto</label>
                                        </div>
                                        <div class="input-group-append">
                                            <button id="hapus1" type="button" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>
                                        </div>
                                    </div>    
                                </form>                                                    
                            </div>
                        </div>
                      </div>
                      <div class="col-md-3">
                      <div class="card">
                        <div id="results2" ></div>
                        <input type="hidden" name="foto2" id="foto2">
                          <div class="card-body">
                                <form id="form2" action="<?php echo site_url('transaksi/upload_foto_2'); ?>" method="post" enctype="multipart/form-data">
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input name="file" type="file" accept="image/*" capture="camera" class="custom-file-input" id="upload_foto2">
                                            <label class="custom-file-label" for="inputGroupFile04">Pilih Foto</label>
                                        </div>
                                        <div class="input-group-append">
                                            <button id="hapus2" type="button" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>
                                        </div>
                                    </div>    
                                </form>                                                    
                            </div>
                      </div>
                      </div>
                      <div class="col-md-3">
                      <div class="card">
                        <div id="results3" ></div>
                        <input type="hidden" name="foto3" id="foto3">
                        <div class="card-body">
                                <form id="form3" action="<?php echo site_url('transaksi/upload_foto_2'); ?>" method="post" enctype="multipart/form-data">
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input name="file" type="file" accept="image/*" capture="camera" class="custom-file-input" id="upload_foto3">
                                            <label class="custom-file-label" for="inputGroupFile04">Pilih Foto</label>
                                        </div>
                                        <div class="input-group-append">
                                            <button id="hapus3" type="button" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>
                                        </div>
                                    </div>    
                                </form>                                                    
                            </div>
                      </div>
                      </div>
                      <div class="col-md-3">
                      <div class="card">
                        <div id="results4" ></div>
                        <input type="hidden" name="foto4" id="foto4">
                          <div class="card-body">
                                <form id="form4" action="<?php echo site_url('transaksi/upload_foto_2'); ?>" method="post" enctype="multipart/form-data">
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input name="file" type="file" accept="image/*" capture="camera" class="custom-file-input" id="upload_foto4">
                                            <label class="custom-file-label" for="inputGroupFile04">Pilih Foto</label>
                                        </div>
                                        <div class="input-group-append">
                                            <button id="hapus4" type="button" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>
                                        </div>
                                    </div>    
                                </form>                                                    
                            </div>
                      </div>
                      </div>
                      <div class="col-md-3">
                      <div class="card">
                        <div id="results5" ></div>
                        <input type="hidden" name="foto5" id="foto5">
                          <div class="card-body">
                                <form id="form5" action="<?php echo site_url('transaksi/upload_foto_2'); ?>" method="post" enctype="multipart/form-data">
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input name="file" type="file" accept="image/*" capture="camera" class="custom-file-input" id="upload_foto5">
                                            <label class="custom-file-label" for="inputGroupFile04">Pilih Foto</label>
                                        </div>
                                        <div class="input-group-append">
                                            <button id="hapus5" type="button" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>
                                        </div>
                                    </div>    
                                </form>                                                    
                            </div>
                      </div>
                      </div>
                    </div>

        
                            
                          
                    </div>
                </div>
            </div>
        </div>

        </div>
        <!-- end data -->
        <!-- foto -->
      </div>
    </section>
</div>

<div id="modal_hapus" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Hapus Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="modal-body" class="modal-body">
        <p>Yakin ingin menghapus data ini ?</p>
      </div>
      <div class="modal-footer">
        <form method="post" action="<?php echo site_url('transaksi/hapus_detail/')?>">
          <input type="hidden" value="<?php echo $h->IdPermohonan?>" id="id" name="id">
          <input type="hidden" id="app" name="app">
          <button type="submit" class="btn btn-primary">Hapus</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        </form>
      </div>
    </div>
  </div>
</div>


<div id="modal_foto1" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!--  -->
      <div id="my_camera1"></div>
      <input type=button value="Take Snapshot" id="ambil_gambar1">
      <!--  -->
    </div>
  </div>
</div>

<div id="modal_foto2" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!--  -->
      <div id="my_camera2"></div>
      <input type=button value="Take Snapshot" id="ambil_gambar2">
      <!--  -->
    </div>
  </div>
</div>

<div id="modal_foto3" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!--  -->
      <div id="my_camera3"></div>
      <input type=button value="Take Snapshot" id="ambil_gambar3">
      <!--  -->
    </div>
  </div>
</div>

<div id="modal_foto4" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!--  -->
      <div id="my_camera4"></div>
      <input type=button value="Take Snapshot" id="ambil_gambar4">
      <!--  -->
    </div>
  </div>
</div>

<div id="modal_foto5" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!--  -->
      <div id="my_camera5"></div>
      <input type=button value="Take Snapshot" id="ambil_gambar5">
      <!--  -->
    </div>
  </div>
</div>

<?php $this->load->view('footer'); ?>
<script type="text/javascript" src="<?php echo base_url('assets')?>/dist/js/webcam.min.js"></script>

<script>
 
$(document).ready(function(){
   
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        } else {
          event.preventDefault();
          // mulai
            $.ajax({
              url       : '<?php echo site_url('transaksi/set_data') ?>',
              data      : $('#set_data').serialize(),
              type      :'POST',
              dataType  : 'JSON',
              beforeSend: function() {
                // $("textarea").attr("disabled",true);
                // $("button").attr("disabled",true);
              },
              complete:function() {
                // $("textarea").attr("disabled",false);
                // $("button").attr("disabled",false);								
              },
              success:function(hasil) {
                // if (hasil.status == 500)
                // {
                //   console.log(hasil)
                // } 
                // else if (hasil.status == 200)
                // {
                //   console.log(hasil);
                //   $('#foto').show();
                //   $('#data').hide();
                // }
              }
            })
          
        }
        form.classList.add('was-validated');
      }, false);
    });

    
    $('#hapus1').on('click', function() {
      //hapus(1)
      $('#app').val(1)
      $('#modal_hapus').modal('show')
    })

    $('#hapus2').on('click', function() {
      //hapus(1)
      $('#app').val(2)
      $('#modal_hapus').modal('show')
    })

    $('#hapus3').on('click', function() {
      //hapus(1)
      $('#app').val(3)
      $('#modal_hapus').modal('show')
    })

    $('#hapus4').on('click', function() {
      //hapus(1)
      $('#app').val(4)
      $('#modal_hapus').modal('show')
    })

    $('#hapus5').on('click', function() {
      //hapus(1)
      $('#app').val(5)
      $('#modal_hapus').modal('show')
    })

    function readURL(input, foto) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                document.getElementById('results' + foto).innerHTML =
                        '<img class="img-fluid" src=" ' + e.target.result + '"/>';
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $('#upload_foto1').on('change', function() {
        //
        var fd = new FormData();
        var files = $('#upload_foto1')[0].files[0];
        fd.append('file',files);
        fd.append('id',<?php echo $h->IdPermohonan?>);
        fd.append('app', 1);

        $.ajax({
            url       : "<?php echo site_url('transaksi/upload_foto_2'); ?>",
            data      : fd,
            type      :'POST',
            dataType  : 'JSON',
            contentType: false,
            processData: false,
            success:function(hasil) {
                Swal.fire({
                  title: hasil.status,
                  showConfirmButton: false,
                  timer: 2000
                })
            }
        })
        readURL(this, 1)

    })

    $('#upload_foto2').on('change', function() {
        //
        var fd = new FormData();
        var files = $('#upload_foto2')[0].files[0];
        fd.append('file',files);
        fd.append('id',<?php echo $h->IdPermohonan?>);
        fd.append('app', 2);

        $.ajax({
            url       : "<?php echo site_url('transaksi/upload_foto_2'); ?>",
            data      : fd,
            type      :'POST',
            dataType  : 'JSON',
            contentType: false,
            processData: false,
            success:function(hasil) {
                Swal.fire({
                  title: hasil.status,
                  showConfirmButton: false,
                  timer: 2000
                })
            }
        })
        readURL(this, 2)

    })

    $('#upload_foto3').on('change', function() {
        //
        var fd = new FormData();
        var files = $('#upload_foto3')[0].files[0];
        fd.append('file',files);
        fd.append('id',<?php echo $h->IdPermohonan?>);
        fd.append('app', 3);

        $.ajax({
            url       : "<?php echo site_url('transaksi/upload_foto_2'); ?>",
            data      : fd,
            type      :'POST',
            dataType  : 'JSON',
            contentType: false,
            processData: false,
            success:function(hasil) {
                Swal.fire({
                  title: hasil.status,
                  showConfirmButton: false,
                  timer: 2000
                })
            }
        })
        readURL(this, 3)

    })

    $('#upload_foto4').on('change', function() {
        //
        var fd = new FormData();
        var files = $('#upload_foto4')[0].files[0];
        fd.append('file',files);
        fd.append('id',<?php echo $h->IdPermohonan?>);
        fd.append('app', 4);

        $.ajax({
            url       : "<?php echo site_url('transaksi/upload_foto_2'); ?>",
            data      : fd,
            type      :'POST',
            dataType  : 'JSON',
            contentType: false,
            processData: false,
            success:function(hasil) {
                Swal.fire({
                  title: hasil.status,
                  showConfirmButton: false,
                  timer: 2000
                })
            }
        })
        readURL(this, 4)

    })

    $('#upload_foto5').on('change', function() {
        //
        var fd = new FormData();
        var files = $('#upload_foto5')[0].files[0];
        fd.append('file',files);
        fd.append('id',<?php echo $h->IdPermohonan?>);
        fd.append('app', 5);

        $.ajax({
            url       : "<?php echo site_url('transaksi/upload_foto_2'); ?>",
            data      : fd,
            type      :'POST',
            dataType  : 'JSON',
            contentType: false,
            processData: false,
            success:function(hasil) {
                Swal.fire({
                  title: hasil.status,
                  showConfirmButton: false,
                  timer: 2000
                })
            }
        })
        readURL(this, 5)

    })
    

   
      $.ajax({
        url       : "<?php echo site_url('transaksi/get_detail_transaksi'); ?>",
        data      : {
          id : $('#id').val(),
        },
        type      :'POST',
        dataType  : 'JSON',
        success:function(hasil) {
          console.log()
          for (var x=0; x<hasil.d.length; x++)
          {
            document.getElementById('results' + hasil.d[x].App).innerHTML =
                        '<img class="img-fluid" src="<?php echo base_url('assets/')?>' + hasil.d[x].Dokumen + '"/>';
          }
        }
      })
    

});

</script>


</body>
</html>
<!-- ./wrapper -->

