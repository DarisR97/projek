
  <!-- Isi Konten -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Lengkapi Berkas</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
          
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <!-- foto -->
         <!-- data -->
         <div id="foto">
        <div class="row">
            <div class="col-12">
                <div class="card">
                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item active" aria-current="page">lengkapi berkas</li>
                        </ol>
                      </nav>
                      <input type="file" accept="image/*" capture="camera">
<input type="file" accept="video/*" capture="camcorder">
<input type="file" accept="audio/*" capture="microphone">
                    <div class="card-body">
                          
<!--                           
                    <div class='row'>
                      <div class="col-12">
                      <div class="input-group">
                            <input type="submit" class="btn btn-sm btn-primary btn-block" value="simpan transaksi">
                          </div>
                      </div>
                    </div> -->
                    
                    <div class="row">
                     
                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box bg-light">
                          <div class="info-box-content">
                            <span class="info-box-text text-center text-muted">No Permohonan</span>
                            <span class="info-box-number text-center text-muted mb-0"><?php echo $h->NoPermohonan ?></span>
                          </div>
                        </div>
                      </div>
                   
                      <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box bg-light">
                          <div class="info-box-content">
                            <span class="info-box-text text-center text-muted">Tanggal Pengajuan</span>
                            <span class="info-box-number text-center text-muted mb-0"><?php echo $h->TglPermohonan ?></span>
                          </div>
                        </div>
                      </div>

                      <!-- fix for small devices only -->
                      <div class="clearfix hidden-md-up"></div>

                      <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box bg-light">
                          <div class="info-box-content">
                            <span class="info-box-text text-center text-muted">Nama</span>
                            <span class="info-box-number text-center text-muted mb-0"><?php echo $h->UserId ?></span>
                          </div>
                        </div>
                      </div>
                      <!-- /.col -->
                     
                      <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box bg-light">
                          <div class="info-box-content">
                            <span class="info-box-text text-center text-muted">Catatan Kegiatan</span>
                            <span class="info-box-number text-center text-muted mb-0"><?php echo $h->CatatanKegiatan ?></span>
                          </div>
                        </div>
                      </div>

                    </div>

                    <div class="row">
                      <div class="col-md-3">
                      <div class="card">
                        <div id="results1" ></div>
                        <input type="hidden" name="foto1" id="foto1">
                        <div class="card-body">
                          <div class="btn-group btn-block" role="group" aria-label="Basic example">
                            <button id="popup_foto1" type="button" class="btn btn-secondary"><i class="fa fa-camera" aria-hidden="true"></i> Foto</button>
                            <!-- <span class="btn btn-secondary fileinput-button">
                              <i class="fa fa-folder" aria-hidden="true"></i>
                              <span>files</span>
                                <input id="file" onchange="save_choosen_file(1)" type="file" name="file"/>
                            </span> -->
                            <button id="hapus1" type="button" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>
                          </div>
                        </div>
                      </div>
                      </div>
                      <div class="col-md-3">
                      <div class="card">
                        <div id="results2" ></div>
                        <input type="hidden" name="foto2" id="foto2">
                        <div class="card-body">
                          <div class="btn-group btn-block" role="group" aria-label="Basic example">
                            <button id="popup_foto2" type="button" class="btn btn-secondary"><i class="fa fa-camera" aria-hidden="true"></i> Foto</button>
                            <!-- <span class="btn btn-secondary fileinput-button">
                              <i class="fa fa-folder" aria-hidden="true"></i>
                              <span>files</span>
                                <input id="file" onchange="save_choosen_file(1)" type="file" name="file"/>
                            </span> -->
                            <button id="hapus2" type="button" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>
                          </div>
                        </div>
                      </div>
                      </div>
                      <div class="col-md-3">
                      <div class="card">
                        <div id="results3" ></div>
                        <input type="hidden" name="foto3" id="foto3">
                        <div class="card-body">
                          <div class="btn-group btn-block" role="group" aria-label="Basic example">
                            <button id="popup_foto3" type="button" class="btn btn-secondary"><i class="fa fa-camera" aria-hidden="true"></i> Foto</button>
                            <!-- <span class="btn btn-secondary fileinput-button">
                              <i class="fa fa-folder" aria-hidden="true"></i>
                              <span>files</span>
                                <input id="file" onchange="save_choosen_file(1)" type="file" name="file"/>
                            </span> -->
                            <button id="hapus3" type="button" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>
                          </div>
                        </div>
                      </div>
                      </div>
                      <div class="col-md-3">
                      <div class="card">
                        <div id="results4" ></div>
                        <input type="hidden" name="foto4" id="foto4">
                        <div class="card-body">
                        <div class="btn-group btn-block" role="group" aria-label="Basic example">
                            <button id="popup_foto4" type="button" class="btn btn-secondary"><i class="fa fa-camera" aria-hidden="true"></i> Foto</button>
                            <!-- <span class="btn btn-secondary fileinput-button">
                              <i class="fa fa-folder" aria-hidden="true"></i>
                              <span>files</span>
                                <input id="file" onchange="save_choosen_file(1)" type="file" name="file"/>
                            </span> -->
                            <button id="hapus4" type="button" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>
                          </div>
                        </div>
                      </div>
                      </div>
                      <div class="col-md-3">
                      <div class="card">
                        <div id="results5" ></div>
                        <input type="hidden" name="foto5" id="foto5">
                        <div class="card-body">
                          <div class="btn-group btn-block" role="group" aria-label="Basic example">
                            <button id="popup_foto5" type="button" class="btn btn-secondary"><i class="fa fa-camera" aria-hidden="true"></i> Foto</button>
                            <!-- <span class="btn btn-secondary fileinput-button">
                              <i class="fa fa-folder" aria-hidden="true"></i>
                              <span>files</span>
                                <input id="file" onchange="save_choosen_file(1)" type="file" name="file"/>
                            </span> -->
                            <button id="hapus5" type="button" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>
                          </div>
                        </div>
                      </div>
                      </div>
                    </div>

        
                            
                          
                    </div>
                </div>
            </div>
        </div>

        </div>
        <!-- end data -->
        <!-- foto -->
      </div>
    </section>
</div>

<div id="modal_hapus" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Hapus Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="modal-body" class="modal-body">
        <p>Yakin ingin menghapus data ini ?</p>
      </div>
      <div class="modal-footer">
        <form method="post" action="<?php echo site_url('transaksi/hapus_detail/')?>">
          <input type="hidden" value="<?php echo $h->IdPermohonan?>" id="id" name="id">
          <input type="hidden" id="app" name="app">
          <button type="submit" class="btn btn-primary">Hapus</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        </form>
      </div>
    </div>
  </div>
</div>


<div id="modal_foto1" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!--  -->
      <div id="my_camera1"></div>
      <input type=button value="Take Snapshot" id="ambil_gambar1">
      <!--  -->
    </div>
  </div>
</div>

<div id="modal_foto2" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!--  -->
      <div id="my_camera2"></div>
      <input type=button value="Take Snapshot" id="ambil_gambar2">
      <!--  -->
    </div>
  </div>
</div>

<div id="modal_foto3" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!--  -->
      <div id="my_camera3"></div>
      <input type=button value="Take Snapshot" id="ambil_gambar3">
      <!--  -->
    </div>
  </div>
</div>

<div id="modal_foto4" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!--  -->
      <div id="my_camera4"></div>
      <input type=button value="Take Snapshot" id="ambil_gambar4">
      <!--  -->
    </div>
  </div>
</div>

<div id="modal_foto5" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!--  -->
      <div id="my_camera5"></div>
      <input type=button value="Take Snapshot" id="ambil_gambar5">
      <!--  -->
    </div>
  </div>
</div>

<?php $this->load->view('footer'); ?>
<script type="text/javascript" src="<?php echo base_url('assets')?>/dist/js/webcam.min.js"></script>

<script>
 
$(document).ready(function(){
   
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        } else {
          event.preventDefault();
          // mulai
            $.ajax({
              url       : '<?php echo site_url('transaksi/set_data') ?>',
              data      : $('#set_data').serialize(),
              type      :'POST',
              dataType  : 'JSON',
              beforeSend: function() {
                // $("textarea").attr("disabled",true);
                // $("button").attr("disabled",true);
              },
              complete:function() {
                // $("textarea").attr("disabled",false);
                // $("button").attr("disabled",false);								
              },
              success:function(hasil) {
                // if (hasil.status == 500)
                // {
                //   console.log(hasil)
                // } 
                // else if (hasil.status == 200)
                // {
                //   console.log(hasil);
                //   $('#foto').show();
                //   $('#data').hide();
                // }
              }
            })
          
        }
        form.classList.add('was-validated');
      }, false);
    });

    $('#ambil_gambar1').on('click', function() {
      take_snapshot1()
    })

    $('#ambil_gambar2').on('click', function() {
      take_snapshot2()
    })

    $('#ambil_gambar3').on('click', function() {
      take_snapshot3()
    })

    $('#ambil_gambar4').on('click', function() {
      take_snapshot4()
    })

    $('#ambil_gambar5').on('click', function() {
      take_snapshot5()
    })

    function take_snapshot1() {
    
        // take snapshot and get image data
        Webcam.snap(function (data_uri) {                    
            document.getElementById('results1').innerHTML =
                            '<img class="img-fluid" src="' + data_uri + '"/>';
            $('#foto1').val(data_uri);
            Webcam.upload( data_uri, '<?php echo site_url('transaksi/upload_foto/' . 1 . '/' . $h->IdPermohonan); ?>', function(code, text) {
                if (code == '200') {
                    $('#modal_foto1').modal('hide');
                } else {
                    alert('error');
                }
            } );
        });
    }

    function take_snapshot2() {
    
      // take snapshot and get image data
      Webcam.snap(function (data_uri) {                    
          document.getElementById('results2').innerHTML =
                          '<img class="img-fluid" src="' + data_uri + '"/>';
          $('#foto2').val(data_uri);
          Webcam.upload( data_uri, '<?php echo site_url('transaksi/upload_foto/' . 2 . '/' . $h->IdPermohonan); ?>', function(code, text) {
              if (code == '200') {
                  $('#modal_foto2').modal('hide');
              } else {
                  alert('error');
              }
          } );
      });
    }

    function take_snapshot3() {
    
    // take snapshot and get image data
    Webcam.snap(function (data_uri) {                    
        document.getElementById('results3').innerHTML =
                        '<img class="img-fluid" src="' + data_uri + '"/>';
        $('#foto3').val(data_uri);
        Webcam.upload( data_uri, '<?php echo site_url('transaksi/upload_foto/' . 3 . '/' . $h->IdPermohonan); ?>', function(code, text) {
            if (code == '200') {
                $('#modal_foto3').modal('hide');
            } else {
                alert('error');
            }
        } );
    });
  }

  function take_snapshot4() {
    
    // take snapshot and get image data
    Webcam.snap(function (data_uri) {                    
        document.getElementById('results4').innerHTML =
                        '<img class="img-fluid" src="' + data_uri + '"/>';
        $('#foto4').val(data_uri);
        Webcam.upload( data_uri, '<?php echo site_url('transaksi/upload_foto/' . 4 . '/' . $h->IdPermohonan); ?>', function(code, text) {
            if (code == '200') {
                $('#modal_foto4').modal('hide');
            } else {
                alert('error');
            }
        } );
    });
  }

  function take_snapshot5() {
    
    // take snapshot and get image data
    Webcam.snap(function (data_uri) {                    
        document.getElementById('results5').innerHTML =
                        '<img class="img-fluid" src="' + data_uri + '"/>';
        $('#foto5').val(data_uri);
        Webcam.upload( data_uri, '<?php echo site_url('transaksi/upload_foto/' . 5 . '/' . $h->IdPermohonan); ?>', function(code, text) {
            if (code == '200') {
                $('#modal_foto5').modal('hide');
            } else {
                alert('error');
            }
        } );
    });
  }

    $('#popup_foto1').on('click', function() {
        $('#modal_foto1').modal('show');

         //foto
          Webcam.set({
              width: 520,
              height: 440,
              image_format: 'jpeg',
              jpeg_quality: 100
          });

          Webcam.attach( '#my_camera1' );
    })

    $('#popup_foto2').on('click', function() {
        $('#modal_foto2').modal('show');

           //foto
           Webcam.set({
              width: 520,
              height: 440,
              image_format: 'jpeg',
              jpeg_quality: 100
          });

          Webcam.attach( '#my_camera2' );
    })

    $('#popup_foto3').on('click', function() {
        $('#modal_foto3').modal('show');

           //foto
           Webcam.set({
              width: 520,
              height: 440,
              image_format: 'jpeg',
              jpeg_quality: 100
          });

          Webcam.attach( '#my_camera3' );
    })

    $('#popup_foto4').on('click', function() {
        $('#modal_foto4').modal('show');

           //foto
           Webcam.set({
              width: 520,
              height: 440,
              image_format: 'jpeg',
              jpeg_quality: 100
          });

          Webcam.attach( '#my_camera4' );
    })

    $('#popup_foto5').on('click', function() {
        $('#modal_foto5').modal('show');

           //foto
           Webcam.set({
              width: 520,
              height: 440,
              image_format: 'jpeg',
              jpeg_quality: 100
          });

          Webcam.attach( '#my_camera5' );
    })
    
    $('#hapus1').on('click', function() {
      //hapus(1)
      $('#app').val(1)
      $('#modal_hapus').modal('show')
    })

    $('#hapus2').on('click', function() {
      //hapus(1)
      $('#app').val(2)
      $('#modal_hapus').modal('show')
    })

    $('#hapus3').on('click', function() {
      //hapus(1)
      $('#app').val(3)
      $('#modal_hapus').modal('show')
    })

    $('#hapus4').on('click', function() {
      //hapus(1)
      $('#app').val(4)
      $('#modal_hapus').modal('show')
    })

    $('#hapus5').on('click', function() {
      //hapus(1)
      $('#app').val(5)
      $('#modal_hapus').modal('show')
    })
    
    // function hapus(app_)
    // {
    //   $.ajax({
    //     url       : "<?php echo site_url('transaksi/hapus_detail'); ?>",
    //     data      : {
    //       id : $('#id').val(),
    //       app : app_,
    //     },
    //     type      :'POST',
    //     dataType  : 'JSON',
    //     beforeSend: function() {
    //     // $("textarea").attr("disabled",true);
    //     // $("button").attr("disabled",true);
    //     },
    //     complete:function() {
    //     // $("textarea").attr("disabled",false);
    //     // $("button").attr("disabled",false);								
    //     },
    //     success:function(hasil) {
         
    //     }
    //   })
    // }
      $.ajax({
        url       : "<?php echo site_url('transaksi/get_detail_transaksi'); ?>",
        data      : {
          id : $('#id').val(),
        },
        type      :'POST',
        dataType  : 'JSON',
        success:function(hasil) {
          console.log()
          for (var x=0; x<hasil.d.length; x++)
          {
            document.getElementById('results' + hasil.d[x].App).innerHTML =
                        '<img class="img-fluid" src="<?php echo base_url('assets/')?>' + hasil.d[x].Dokumen + '"/>';
          }
        }
      })
    

});

</script>


</body>
</html>
<!-- ./wrapper -->

