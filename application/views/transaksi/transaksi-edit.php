
  <!-- Isi Konten -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit Pengajuan Permohonan</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
          
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <!-- data -->
        <div id="data">
        <div class="row">
            <div class="col-12">
                <div class="card">
                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item active" aria-current="page">Isi form untuk pengajuan</li>
                        </ol>
                      </nav>

                    <div class="card-body">
                    <?php if ($this->session->set_flashdata('message')) echo $this->session->set_flashdata('success') ?>
                    <form class="needs-validation" id="set_data" novalidate>
                      <input id="IdPermohonan" name="IdPermohonan" type="hidden" value="<?php echo $h->IdPermohonan?> ">
                      <div class="form-row">
                        <div class="col-md-3 mb-3">
                          <label for="validationCustom01">No Permohonan</label>
                          <input value="<?php echo $h->NoPermohonan?>" name="no_permohonan" placeholder="No Permohonan" type="text" class="form-control" id="validationCustom01" placeholder="First name" required>
                          <div class="invalid-feedback">
                            Isi nomor permohonan
                          </div>
                        </div>
                        <div class="col-md-3 mb-3">
                          <label for="validationCustom02">Tgl Permohonan</label>
                            <div class="form-group">
                                <div class="input-group date" id="datetimepicker4" data-target-input="nearest">
                                    <input value="<?php echo $h->TglPermohonan?>" name="tgl_permohonan" placeholder="DD - MM - YYYY" name="tgl_permohonan" type="text" class="form-control datetimepicker-input" data-target="#datetimepicker4"/>
                                    <div class="input-group-append" data-target="#datetimepicker4" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                          <label for="validationCustomUsername">Nama</label>
                          <div class="input-group">
                            <input value="<?php echo $this->session->userdata('id_user')?>" name="nama" type="hidden" class="form-control" id="validationCustomUsername" placeholder="Nama" aria-describedby="inputGroupPrepend" required>
                            <input readonly value="<?php echo $this->session->userdata('nama')?>" type="text" class="form-control" id="validationCustomUsername" placeholder="Nama" aria-describedby="inputGroupPrepend" required>
                            <div class="invalid-feedback">
                              Isi nama
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3 mb-3">
                          <label for="validationCustom03">Catatan Kegiatan</label>
                          <input value="<?php echo $h->CatatanKegiatan; ?>" name="catatan_kegiatan" type="text" class="form-control" id="validationCustom03" placeholder="Catatan Kegiatan" required>
                          <div class="invalid-feedback">
                            Isi catatan kegiatan
                          </div>
                        </div>
                      </div>
                      
                     
                      <a href="<?php echo site_url('transaksi/tambah')?>" style="float: left;" class="btn btn-secondary"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Kembali</a>
                      <button style="float: right;" class="btn btn-primary" type="submit">Simpan Perubahan <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button>
                    </form>

                        <?php if ($this->session->flashdata('berhasil')) echo $this->session->flashdata('berhasil')  ?>
                        
                    </div>
                </div>
            </div>
        </div>

        </div>
        <!-- end data -->

      </div>
    </section>
</div>



<?php $this->load->view('footer'); ?>
<script type="text/javascript" src="<?php echo base_url('assets')?>/dist/js/webcam.min.js"></script>

<script>
    var table

$(document).ready(function(){
      $('#datetimepicker4').datetimepicker({
        format: 'DD-MM-YYYY'
      });
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        } else {
          event.preventDefault();
          // mulai
            $.ajax({
              url       : '<?php echo site_url('transaksi/edit_set_data') ?>',
              data      : $('#set_data').serialize(),
              type      :'POST',
              dataType  : 'JSON',
              beforeSend: function() {
                // $("textarea").attr("disabled",true);
                // $("button").attr("disabled",true);
              },
              complete:function() {
                // $("textarea").attr("disabled",false);
                // $("button").attr("disabled",false);								
              },
              success:function(hasil) {
                // if (hasil.status == 500)
                // {
                //   console.log(hasil)
                // } 
                // else if (hasil.status == 200)
                // {
                //   window.location.href = "<?php echo site_url('transaksi/lengkapi_berkas/')?>" + hasil.data;
                // }
                $('#pengajuan_table').DataTable().ajax.reload();
                Swal.fire({
                  title: 'Berhasil, silahkan lengkapi berkas anda',
                  showConfirmButton: false,
                  timer: 2000
                })
                $("#set_data").trigger("reset");
              }
            })
          
        }
        form.classList.add('was-validated');
      }, false);
    });

});

</script>


</body>
</html>
<!-- ./wrapper -->

