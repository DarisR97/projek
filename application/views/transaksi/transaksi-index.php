
  <!-- Isi Konten -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Transaksi</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
           
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        
        <div class="row">
            <div class="col-12">
                <div class="card">
                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item active" aria-current="page">daftar pengajuan</li>
                        </ol>
                      </nav>

                    <div class="card-body">
                        <table id="pengajuan_table" class="table table-bordered table-hover table-sm">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Catatan Kegiatan</th>
                                    <th>Tanggal Permohonan</th>
                                    <th>Nama Pemohon</th>
                                    <th>Status</th>
                                    <th style="width: 20%;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                             
                            </body>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </section>
</div>

<div id="modal_hapus" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Hapus Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="modal-body" class="modal-body">
        <p>Yakin ingin menghapus data ini ?</p>
      </div>
      <div class="modal-footer">
        <form method="post" action="<?php echo site_url('transaksi/hapus/')?>">
          <input type="hidden" name="pengajuan_id" id="pengajuan_id">
          <button type="submit" class="btn btn-primary">Hapus</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        </form>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('footer'); ?>

<script>
    var table
    $(document).ready(function(){
        table = $('#pengajuan_table').DataTable({
          "bDestroy": true,
          "processing": true, 
          "serverSide": true,
          "order": [], 
                
          "ajax": {
            "url": "<?php echo site_url('transaksi/ajax_table_transaksi')?>",
            "type": "POST",
          },
      
          "columnDefs": [
            { 
              "targets": [ 0 ], 
              "orderable": false,
              "searchable": false,
            },
          ],
        });

        
    });
</script>


</body>
</html>
<!-- ./wrapper -->

