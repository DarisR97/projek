<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index3.html" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li>
    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>
      <!-- Notifications Dropdown Menu -->
       <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-user"></i> <?php echo $this->session->userdata('nama'); ?>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <a href="<?php echo site_url('auth/logout')?>" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                <i class="fa fa-times"></i> Keluar
                </h3>
              </div>
            </div>
            <!-- Message End -->
          </a>
        
         
        </div>
      </li>
    </ul>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link">
      <span class="brand-text font-weight-light">Beranda</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="info">
          <a href="#" class="d-block">Admin</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <?php
            $menu_id = $this->db->get_where('role_menu_akses', ['role_id' => $this->session->userdata('id_role')]);
            foreach($menu_id->result() as $field)
            {
              $menu = $this->db->get_where('menu', ['id' => $field->menu_id])->row();
              if ($menu->link == '#')
              {
                $submenu = $this->db->get_where('sub_menu', ['id_menu' => $menu->id]);
                echo '
                <li class="nav-item has-treeview">
                  <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-user"></i>
                    <p>
                      Role
                      <i class="right fas fa-angle-left"></i>
                    </p>
                  </a>
                  <ul class="nav nav-treeview"> ';
                  foreach($submenu->result() as $row) {
                  echo '
                    <li class="nav-item">
                      <a href="' .  site_url($row->link) . '" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>' . $row->nama_sub . '</p>
                      </a>
                    </li>';
                    }
                  echo ' </ul>
                </li>
                ';
              }
              else
              {
                echo "
                  <li class='nav-item'>
                  <a href='" . site_url($menu->link) . "' class='nav-link'>
                    <i class='" . $menu->icon . "'></i>
                    <p>
                      " . $menu->menu . "
                    </p>
                  </a>
                  </li>
                ";
              }

            }
          ?>
        </ul>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>