
  <!-- Isi Konten -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Submenu</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Title</h3>
                    </div>

                    <div class="card-body">
                        <?php if ($this->session->flashdata('berhasil')) echo $this->session->flashdata('berhasil')  ?>
                        <form method="post" action="<?php echo site_url('submenu/submit_edit')?>">
                            <input value="<?php echo $submenu->id?>" type="hidden" class="form-control form-control-sm" id="id" name="id" placeholder="Masukan nama submenu">
                            <div class="form-group">
                                <label>Nama Sub</label>
                                <input value="<?php echo $submenu->nama_sub?>" type="text" class="form-control form-control-sm" id="nama_sub" name="nama_sub" placeholder="Masukan nama sub">
                                <small id="help" class="form-text text-muted"><?php echo form_error('nama_sub'); ?></small>
                            </div>
                            <div class="form-group">
                                <label>Link</label>
                                <input value="<?php echo $submenu->link?>" type="text" class="form-control form-control-sm" id="link" name="link" placeholder="Masukan link">
                                <small id="help" class="form-text text-muted"><?php echo form_error('link'); ?></small>
                            </div>
                            <div class="form-group">
                                <label>Pilih Menu</label>
                                <select name="id_menu" id="id_menu" class="custom-select">
                                    <?php 
                                        foreach($menu->result() as $field) {
                                            $selected = '';
                                            if ($field->id == $submenu->id_menu) {
                                                $selected = 'selected';
                                            }   
                                    ?>
                                    <option value='<?php echo $field->id?>' <?php echo $selected?>><?php echo $field->menu?></option>

                                    <?php } ?>
                                </select>
                                <small id="help" class="form-text text-muted"><?php echo form_error('id_menu'); ?></small>
                            </div>
                            <button type="submit" name="tambah" class="btn btn-primary btn-sm">Simpan Data</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </section>
</div>

<?php $this->load->view('footer'); ?>




</body>
</html>
<!-- ./wrapper -->

