<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('ModelAuth');
    }
	
	public function index()
	{
		$this->load->view('auth/auth-index');
    }
    
    public function submit()
    {
        if ($_POST)
        {
            $username   = $this->input->post('username');
            $password   = md5($this->input->post('password'));
            $validation = $this->form_validation;
            $auth       = $this->ModelAuth;

            $validation->set_rules($auth->rules());

            if($validation->run() == false) {
                $this->index();
            } else {
                if ($this->ModelAuth->check_login($username, $password) == true)
                {
                    redirect('home');
                }
                else
                {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Terjadi kesalahan</strong></div>');
                    redirect('auth');
                }
            }
            // cek validasi

           
        }
    }

    public function logout()
    {
        session_destroy();
        redirect('auth');
    }
}
