<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('ModelTransaksi');
		if($this->session->userdata('is_login') != true)
		{
			redirect('auth');
		}
	}
	
	public function index()
	{
		if ($this->session->userdata('id_role') == 1)
		{
			$data['ditolak'] 	= $this->ModelTransaksi->get_count(['Reject' => 1, 'UserId' => $this->session->userdata('id_user')])->num_rows();
			$data['disetujui'] 	= $this->ModelTransaksi->get_count(['App1' => 1, 'App2' => 1, 'App3' => 1, 'App4' => 1, 'App5' => 1, 'UserId' => $this->session->userdata('id_user')])->num_rows();
			$data['semua'] 		= $this->ModelTransaksi->get_count(['UserId' => $this->session->userdata('id_user')])->num_rows();

			$this->load->view('header');
			$this->load->view('side-menu');
			$this->load->view('home/index-pengajuan', $data);
			$this->load->view('footer');
		}
		else if ($this->session->userdata('id_role') == 2)
		{

		}
		else if ($this->session->userdata('id_role') == 3)
		{

		}
		else if ($this->session->userdata('id_role') == 4)
		{
			$data['ditolak'] 	= $this->ModelTransaksi->get_count(['Reject' => 1])->num_rows();
			$data['disetujui'] 	= $this->ModelTransaksi->get_count(['App1' => 1, 'App2' => 1, 'App3' => 1, 'App4' => 1, 'App5' => 1,])->num_rows();
			$data['semua'] 		= $this->ModelTransaksi->get_count([])->num_rows();
			$this->load->view('header');
			$this->load->view('side-menu');
			$this->load->view('home/index-kepala', $data);
			$this->load->view('footer');
		}
		else if ($this->session->userdata('id_role') == 5)
		{

		}
		else if ($this->session->userdata('id_role') == 6)
		{

		}
	
	}
}
