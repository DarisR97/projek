<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna extends CI_Controller {

	
	function __construct() {
		parent:: __construct();
        $this->load->model('ModelPengguna');
        $this->load->model('ModelRole');

        if($this->session->userdata('is_login') != true)
		{
			redirect('auth');
		}
    }
	
	public function index()
	{
        $this->load->view('header');
		$this->load->view('side-menu');
		$this->load->view('pengguna/pengguna-index');
    }

    public function ajax_table_pengguna()
    {
        $list = $this->ModelPengguna->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->nama;
           
			$row[] = "
				<a class='btn btn-sm btn-success' style='color: white;' href='". base_url() . "pengguna/edit/" . base64_encode($field->id) ."' title='Edit PO'><span class='fa fa-edit'></span>Edit</a>
				<a class='btn btn-sm btn-danger' style='color: white;' id='hapus' data-id='". $field->id ."' data-toggle='modal' title='Hapus'><span class='fa fa-trash'></span></a>
			";
 
            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->ModelPengguna->count_all(),
            "recordsFiltered" => $this->ModelPengguna->count_filtered(),
			"data" => $data,
        );
        //output dalam format JSON
		echo json_encode($output);
    }
    
    public function tambah()
    {
		$data['role'] = $this->ModelRole->get();
        $this->load->view('header');
		$this->load->view('side-menu');
		$this->load->view('pengguna/pengguna-add', $data);
    }

    public function submit() {
        $pengguna       = $this->ModelPengguna;
        $validation = $this->form_validation;

        if (isset($_POST['tambah'])) {
            $rules = $pengguna->rules();
            $validation->set_rules($rules);

            if($validation->run() == false) {
                $this->tambah();
            } else {
                $pengguna->add();
                $this->session->set_flashdata('berhasil', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>berhasil ditambahkan</strong></div>');
                redirect('pengguna/tambah');
            }
        }
    }

    public function edit($id)
    {
        $id     	= base64_decode($id);
        $pengguna   = $this->ModelPengguna;

		$data['role'] = $this->ModelRole->get();
        $data['pengguna'] = $pengguna->get_where(['id' => $id])->row(0);
        //echo json_encode($data['role']->result());
        $this->load->view('header');
		$this->load->view('side-menu');
		$this->load->view('pengguna/pengguna-edit', $data);
    }

    public function submit_edit() {
        $pengguna       = $this->ModelPengguna;
        $validation = $this->form_validation;

        if (isset($_POST['tambah'])) {
            $rules = $pengguna->rules_ubah();
            $validation->set_rules($rules);

            if($validation->run() == false) {
                $id = $this->input->post('id');
                $this->edit(base64_encode($id));
            } else {
                $pengguna->edit();
                $this->session->set_flashdata('berhasil', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Jabatan berhasil ditambahkan</strong></div>');
                redirect('pengguna');
            }
        }
    }

    public function hapus()
    {
        $id         	= $this->input->post('pengguna_id');
        $pengguna       = $this->ModelPengguna;

        if ($id != '')
        {
            $pengguna->id = $id;
            $pengguna->delete();
        }

        redirect('pengguna');
	}
	
	public function cek_password($data)
	{
		$id 		= $_POST['id'];
		$password 	= md5($_POST['password_sekarang']);

		$user = $this->ModelPengguna->get_where(['id' => $id])->row();
		if ($password == $user->password)
		{
			return true;
		}
		else
		{
			return false;
		}

	}

}
