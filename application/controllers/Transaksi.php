<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends CI_Controller {

    function __construct() {
		parent:: __construct();
        $this->load->model('ModelTransaksi');

        if($this->session->userdata('is_login') != true)
		{
			redirect('auth');
		}
    }
	
	
	public function index()
	{
		$this->load->view('header');
		$this->load->view('side-menu');
		$this->load->view('transaksi/transaksi-index');
		$this->load->view('footer');
    }
    
    public function tambah()
    {
        // // set array to default
        // $array = array(
        //     'no_permohonan'     => '',
        //     'tgl_permohonan'    => '',
        //     'nama'              => '',
        //     'catatan_kegiatan'  => '',
        // );

        // $this->session->set_userdata($array);

        $this->load->view('header');
		$this->load->view('side-menu');
		$this->load->view('transaksi/transaksi-add');
    }

    public function submit()
    {
        echo json_encode($_POST);
    }

    public function upload_foto($app, $id)
    {
        $filename= date('d-m-Y') . '-' . $id . '-image-'. $app .'.jpg';
        $filepath=FCPATH.'assets/'.$filename;
        $result=move_uploaded_file($_FILES['webcam']['tmp_name'], $filepath);

        $array_detail = array(
            'IdPermohonan'  => $id,
            'NoAnggaran'    => 1,
            'Dokumen'       => $filename,
            'App'           => $app, 
        );

        $this->db->insert('trxanggarand', $array_detail);
        
        echo json_encode(['status' => 200]);   

    }

    public function set_data()
    {
        $transaksi       = $this->ModelTransaksi;
        $validation      = $this->form_validation;

        if ($_POST)
        {
            $rules = $transaksi->rules_data();
            $validation->set_rules($rules);

            if($validation->run() == false)
            {
                echo json_encode(['status' => 500, 'error' => validation_errors()]);
            }
            else
            {
                $no         = $this->input->post('no_permohonan');
                $tanggal    = $this->input->post('tgl_permohonan');
                $nama       = $this->input->post('nama');
                $catatan    = $this->input->post('catatan_kegiatan');
    
                $simpan = array(
                    'NoPermohonan'     => $no,
                    'TglPermohonan'    => $tanggal,
                    'UserId'           => $nama,
                    'CatatanKegiatan'  => $catatan,
                );
    
                $this->db->insert('trxanggaranh', $simpan);
            
                $this->session->set_flashdata('message', '
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>Holy guacamole!</strong> Pengajuan selesai diajukan, silahkan lengkapi berkas berikut
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                ');
                echo json_encode(['status' => 200]);   
            }
        }
    }

    public function edit_set_data()
    {
        $transaksi       = $this->ModelTransaksi;
        $validation      = $this->form_validation;

        if ($_POST)
        {
            $rules = $transaksi->rules_data();
            $validation->set_rules($rules);

            if($validation->run() == false)
            {
                echo json_encode(['status' => 500, 'error' => validation_errors()]);
            }
            else
            {
                $no         = $this->input->post('no_permohonan');
                $tanggal    = $this->input->post('tgl_permohonan');
                $nama       = $this->input->post('nama');
                $catatan    = $this->input->post('catatan_kegiatan');

                $simpan = array(
                    'NoPermohonan'     => $no,
                    'TglPermohonan'    => $tanggal,
                    'UserId'           => $nama,
                    'CatatanKegiatan'  => $catatan,
                );
                
                $this->db->where('IdPermohonan', $this->input->post('IdPermohonan'));
                $this->db->update('trxanggaranh', $simpan);
                
                $this->session->set_flashdata('message', '
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>Holy guacamole!</strong> Pengajuan selesai diajukan, silahkan lengkapi berkas berikut
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                ');
                echo json_encode(['status' => 200]);   
            }
        }
    }

    public function lengkapi_berkas($id)
    {
        $id = base64_decode($id);
        $model = $this->ModelTransaksi;

        $data['h'] = $model->get_where(['idPermohonan' => $id])->row();
        $this->load->view('header');
		$this->load->view('side-menu');
        $this->load->view('transaksi/transaksi-lengkapi', $data);
    }

    // public function simpan_transaksi()
    // {
    //     if ($_POST)
    //     {
    //         $array_head = array(
    //             'NoPermohonan'      => $this->session->userdata('no_permohonan'),
    //             'TglPermohonan'     => $this->session->userdata('tgl_permohonan'),
    //             'UserId'            => $this->session->userdata('nama'),
    //             'CatatanKegiatan'   => $this->session->userdata('catatan_kegiatan'),
    //         );

    //         $filename= date('d-m-Y') . $this->session->userdata('no_permohonan') . '-image.jpg';
    //         $filepath=FCPATH.'assets/'.$filename;
    //         $result=move_uploaded_file($_FILES['webcam']['tmp_name'], $filepath);

    //         $this->db->insert('trxanggaranh', $array_head);
    //         $latest_id = $this->db->insert_id();
    //         for ($x=1; $x<=5; $x++) {
    //             $foto = $_POST['foto' . $x];
    //             $array_detail = array(
    //                 'IdPermohonan'  => $latest_id,
    //                 'NoAnggaran'    =>  1,
    //                 'Dokumen'       =>  1,
    //             );
    //             $this->db->insert('trxanggarand', $array_detail);
    //         }
    //     }
    // }
    public function ajax_table_transaksi()
    {
        $list = $this->ModelTransaksi->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $status = 'Dalam Proses';
            if ($field->App1 == '1' && $field->App2 == '1' && $field->App3 == '1' && $field->App4 == '1' && $field->App5 == '1')
            {
                $status = "Disetujui";
            }

            if ($field->Reject == 1)
            {
                $status = "Ditolak";
            }

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->CatatanKegiatan;
            $row[] = $field->TglPermohonan;
            $row[] = $field->nama;
            $row[] = $status;
			$row[] = "
                <a class='btn btn-sm btn-primary' style='color: white;' href='". base_url() . "transaksi/detail/" . base64_encode($field->IdPermohonan) ."' title='Edit PO'><span class='fa fa-check'></span> Setujui</a>
			";
 
            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->ModelTransaksi->count_all(),
            "recordsFiltered" => $this->ModelTransaksi->count_filtered(),
			"data" => $data,
        );
        //output dalam format JSON
		echo json_encode($output);
    }

    public function ajax_table_transaksi_pengajuan()
    {
        $list = $this->ModelTransaksi->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $status = 'Dalam Proses';
            if ($field->App1 == '1' && $field->App2 == '1' && $field->App3 == '1' && $field->App4 == '1' && $field->App5 == '1')
            {
                $status = "Disetujui";
            }

            if ($field->Reject == 1)
            {
                $status = "Ditolak";
            }

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->CatatanKegiatan;
            $row[] = $field->TglPermohonan;
            $row[] = $field->nama;
            $row[] = $status;
           
			$row[] = "
                <a class='btn btn-sm btn-warning'  href='". base_url() . "transaksi/lengkapi_berkas/" . base64_encode($field->IdPermohonan) ."' title='Edit PO'><span class='fa fa-check'></span> Lengkapi</a>
                <a class='btn btn-sm btn-success' style='color: white;' href='". base_url() . "transaksi/edit/" . base64_encode($field->IdPermohonan) ."' title='Edit PO'><span class='fa fa-edit'></span> Edit</a>
				<a class='btn btn-sm btn-danger' style='color: white;' id='hapus' data-id='". $field->IdPermohonan ."' data-toggle='modal' title='Hapus'><span class='fa fa-trash'></span></a>
                <a class='btn btn-sm btn-secondary'  href='". base_url() . "transaksi/lihat_hasil/" . base64_encode($field->IdPermohonan) ."' title='Edit PO'><span class='fa fa-eye'></span> Lihat Hasil</a>
            ";
 
            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->ModelTransaksi->count_all(),
            "recordsFiltered" => $this->ModelTransaksi->count_filtered(),
			"data" => $data,
        );
        //output dalam format JSON
		echo json_encode($output);
    }

    public function detail($id)
    {
        $idPermohonan = base64_decode($id);
        $data['h'] = $this->ModelTransaksi->get_where_join(['IdPermohonan' => $idPermohonan])->row();
        $data['d'] = $this->ModelTransaksi->get_transaksi_detail(['IdPermohonan' => $idPermohonan])->result();

        $this->load->view('header');
		$this->load->view('side-menu');
        $this->load->view('transaksi/transaksi-setujui', $data);
        // echo json_encode($data['d']);
    }

    public function setujui()
    {
        if ($_POST)
        {
            $id = $_POST['id_permohonan'];
            $cek = $_POST['cek'];

            //get status
            $status = $this->ModelTransaksi->get_where(['IdPermohonan' => $id])->row();

           
            if ($cek == 1)
            {   
                if ($status->App1 == 1)
                {
                    $detail = $this->db->get_where('trxanggarand', ['IdPermohonan' => $id , 'App' => $cek])->row();
                    $data = array(
                        'App1' => 0
                    );
                    //update
                    //get qrcode
                    unlink('assets/qrcode/' . $detail->qrcode);

                    $this->db->where(['IdPermohonan' => $id , 'App' => $cek]);
                    $this->db->update('trxanggarand', ['qrcode' => '']);
                } else {
                    $data = array(
                        'App1' => 1
                    );

                    $qr = $this->qrcode_generate($id, $cek);
                    $this->db->where(['IdPermohonan' => $id , 'App' => $cek]);
                    $this->db->update('trxanggarand', ['qrcode' => $qr]);

                    $detail = $this->db->get_where('trxanggarand', ['IdPermohonan' => $id , 'App' => $cek])->row();
                    $source = 'assets/' . $detail->Dokumen;
                    $wm     = 'assets/qrcode/' . $detail->qrcode;
                    $this->watermark($source, $wm);
                }
             
            } else if ($cek == 2)
            {
                if ($status->App2 == 1)
                {
                    $data = array(
                        'App2' => 0
                    );

                      //get qrcode
                      $detail = $this->db->get_where('trxanggarand', ['IdPermohonan' => $id , 'App' => $cek])->row();
                      unlink('assets/qrcode/' . $detail->qrcode);
  
                      $this->db->where(['IdPermohonan' => $id , 'App' => $cek]);
                      $this->db->update('trxanggarand', ['qrcode' => '']);

                } else {
                    $data = array(
                        'App2' => 1
                    );

                    $qr = $this->qrcode_generate($id, $cek);
                    $this->db->where(['IdPermohonan' => $id , 'App' => $cek]);
                    $this->db->update('trxanggarand', ['qrcode' => $qr]);

                    $detail = $this->db->get_where('trxanggarand', ['IdPermohonan' => $id , 'App' => $cek])->row();
                    $source = 'assets/' . $detail->Dokumen;
                    $wm     = 'assets/qrcode/' . $detail->qrcode;
                    $this->watermark($source, $wm);

                }

            } else if ($cek == 3) {
                 if ($status->App3 == 1)
                {
                    $data = array(
                        'App3' => 0
                    );

                      //get qrcode
                      $detail = $this->db->get_where('trxanggarand', ['IdPermohonan' => $id , 'App' => $cek])->row();
                      unlink('assets/qrcode/' . $detail->qrcode);
  
                      $this->db->where(['IdPermohonan' => $id , 'App' => $cek]);
                      $this->db->update('trxanggarand', ['qrcode' => '']);

                } else {
                    $data = array(
                        'App3' => 1
                    );

                    $qr = $this->qrcode_generate($id, $cek);
                    $this->db->where(['IdPermohonan' => $id , 'App' => $cek]);
                    $this->db->update('trxanggarand', ['qrcode' => $qr]);

                    $detail = $this->db->get_where('trxanggarand', ['IdPermohonan' => $id , 'App' => $cek])->row();
                    $source = 'assets/' . $detail->Dokumen;
                    $wm     = 'assets/qrcode/' . $detail->qrcode;
                    $this->watermark($source, $wm);

                }
            } else if ($cek == 4) {
                if ($status->App4 == 1)
                {
                    $data = array(
                        'App4' => 0
                    );

                      //get qrcode
                      $detail = $this->db->get_where('trxanggarand', ['IdPermohonan' => $id , 'App' => $cek])->row();
                      unlink('assets/qrcode/' . $detail->qrcode);
  
                      $this->db->where(['IdPermohonan' => $id , 'App' => $cek]);
                      $this->db->update('trxanggarand', ['qrcode' => '']);

                } else {
                    $data = array(
                        'App4' => 1
                    );

                    $qr = $this->qrcode_generate($id, $cek);
                    $this->db->where(['IdPermohonan' => $id , 'App' => $cek]);
                    $this->db->update('trxanggarand', ['qrcode' => $qr]);

                    $detail = $this->db->get_where('trxanggarand', ['IdPermohonan' => $id , 'App' => $cek])->row();
                    $source = 'assets/' . $detail->Dokumen;
                    $wm     = 'assets/qrcode/' . $detail->qrcode;
                    $this->watermark($source, $wm);
                    
                }
            }
            else if ($cek == 5) {
                if ($status->App5 == 1)
                {
                    $data = array(
                        'App5' => 0
                    );

                      //get qrcode
                      $detail = $this->db->get_where('trxanggarand', ['IdPermohonan' => $id , 'App' => $cek])->row();
                      unlink('assets/qrcode/' . $detail->qrcode);
  
                      $this->db->where(['IdPermohonan' => $id , 'App' => $cek]);
                      $this->db->update('trxanggarand', ['qrcode' => '']);

                } else {
                    $data = array(
                        'App5' => 1
                    );

                    $qr = $this->qrcode_generate($id, $cek);
                    $this->db->where(['IdPermohonan' => $id , 'App' => $cek]);
                    $this->db->update('trxanggarand', ['qrcode' => $qr]);

                    $detail = $this->db->get_where('trxanggarand', ['IdPermohonan' => $id , 'App' => $cek])->row();
                    $source = 'assets/' . $detail->Dokumen;
                    $wm     = 'assets/qrcode/' . $detail->qrcode;
                    $this->watermark($source, $wm);

                }
            }

            $this->ModelTransaksi->setujui_permohonan(['IdPermohonan' => $id], $data);
            echo json_encode(['status' => 'success']);
         
        }
    }

    public function tolak()
    {
        if ($_POST)
        {
            $id = $_POST['id_permohonan'];
            $catatan = $_POST['catatan'];
            $status = $_POST['status'];
            $data = array(
                'Reject' => $status,
                'CatatanReject' => $catatan
            );
            $this->ModelTransaksi->setujui_permohonan(['IdPermohonan' => $id], $data);
            echo json_encode(['status' => 'success']);
        }
    }

    public function hapus()
    {
        if ($_POST)
        {
            $model = $this->ModelTransaksi;
            $id = $this->input->post('pengajuan_id');

            // get data detail
            $detail = $this->db->get_where('trxanggarand', ['idPermohonan' => $id]);
            // remove file
            foreach($detail->result() as $field)
            {
                //delete file
                unlink('./assets/'. $field->Dokumen);
            }

            // hapus detail
            $this->db->delete('trxanggarand', ['idPermohonan' => $id]);
            // hapus header
            $this->db->delete('trxanggaranh', ['idPermohonan' => $id]);

            echo json_encode(['status' => 200]);
            //echo json_encode($detail->result());
        }
    }

    public function upload_file()
    {
       
        // $config['upload_path']="./assets/";
        // $config['allowed_types']='gif|jpeg|png|jpg';
        // $config['encrypt_name'] = TRUE;
         
        // $this->load->library('upload',$config);
        // if($this->upload->do_upload("files")){
        //     echo json_encode("berhasil");
        // } else {
        //     echo json_encode("gagal");
        // }
        echo json_encode($_POST);
    }

    public function hapus_detail()
    {
        $id     = $this->input->post('id');
        $app    = $this->input->post('app');
        // get data detail
        $detail = $this->db->get_where('trxanggarand', ['IdPermohonan' => $id, 'App' => $app])->row();
        // remove file
        //delete file
        unlink('./assets/'. $detail->Dokumen);
        // hapus detail
        $this->db->delete('trxanggarand', ['IdPermohonan' => $id, 'App' => $app]);
        
        redirect('transaksi/lengkapi_berkas/' . base64_encode($id));
    }
    
    public function get_detail_transaksi()
    {
        $id     = $this->input->post('id');
        $detail = $this->db->get_where('trxanggarand', ['idPermohonan' => $id])->result();

        echo json_encode(['d' => $detail]);
    }

    public function edit($id)
    {
        $id         = base64_decode($id);
        $data['h']  = $this->db->get_where('trxanggaranh', ['IdPermohonan' => $id])->row();
        
        $this->load->view('header');
		$this->load->view('side-menu');
		$this->load->view('transaksi/transaksi-edit', $data);
    }

    public function lihat_hasil($id)
    {
        $idPermohonan = base64_decode($id);
        $data['h'] = $this->ModelTransaksi->get_where(['IdPermohonan' => $idPermohonan])->row();
        $data['d'] = $this->ModelTransaksi->get_transaksi_detail(['IdPermohonan' => $idPermohonan])->result();
        
        //$data['dana'] = $this->db->get_where('dana', ['IdPermohonan' => $idPermohonan])->row();

        $this->load->view('header');
		$this->load->view('side-menu');
        $this->load->view('transaksi/transaksi-hasil', $data);
        // echo json_encode($data['dana']);
    }

    public function upload_foto_2()
    {
        
        $id = $this->input->post('id');
        $app = $this->input->post('app');

        $filename= date('d-m-Y') . '-' . $id . '-image-'. $app .'.jpg';

        $config['upload_path']          = './assets';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['file_name']            = $filename;
        $config['overwrite']			= true;
    
        $this->load->library('upload', $config);
    
        if ($this->upload->do_upload('file')) {
            $array_detail = array(
                'IdPermohonan'  => $id,
                'NoAnggaran'    => 1,
                'Dokumen'       => $filename,
                'App'           => $app, 
            );
    
            $this->db->insert('trxanggarand', $array_detail);
            
            echo json_encode(['status' => 'Berhasil']);   
        } else {
            echo json_encode(['status' => $this->upload->display_errors()]);   
        }
        
       
    }

    function qrcode_generate($id, $app){
        // get data informasi untuk di qrcode
        $u = $this->db->get_where('trxanggaranh', ['IdPermohonan' => $id])->row();
        $user = $this->db->get_where('refuser', ['id' => $u->UserId])->row();


        $this->load->library('ciqrcode'); //pemanggilan library QR CODE
 
        $config['cacheable']    = true; //boolean, the default is true
        $config['cachedir']     = './assets/'; //string, the default is application/cache/
        $config['errorlog']     = './assets/'; //string, the default is application/logs/
        $config['imagedir']     = './assets/qrcode/'; //direktori penyimpanan qr code
        $config['quality']      = true; //boolean, the default is true
        $config['size']         = '1024'; //interger, the default is 1024
        $config['black']        = array(224,255,255); // array, default is array(255,255,255)
        $config['white']        = array(70,130,180); // array, default is array(0,0,0)
        $this->ciqrcode->initialize($config);
 
        $image_name = date('d-m-Y') . '-' . $id . '-image-'. $app .'.png';
        
        $params['data'] = $user->nama . ' PERMOHONAN ' . $id . ' APP ' . $app; //data yang akan di jadikan QR CODE
        $params['level'] = 'H'; //H=High
        $params['size'] = 10;
        $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
        $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
        
        return $image_name;
    }

    public function watermark($source, $wm)
    {
        $imgConfig = array();
        $imgConfig['image_library']     = 'GD2';
        $imgConfig['source_image']      = $source;
        $imgConfig['wm_type']           = 'overlay';
        $imgConfig['wm_overlay_path']   = $wm;
                                
        $this->load->library('image_lib', $imgConfig);
        $this->image_lib->initialize($imgConfig);
        $this->image_lib->watermark(); 
    }
    
}
