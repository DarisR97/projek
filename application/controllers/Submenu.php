<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Submenu extends CI_Controller {

    function __construct() {
		parent:: __construct();
        $this->load->model('ModelSubMenu');
        $this->load->model('ModelMenu');

        if($this->session->userdata('is_login') != true)
		{
			redirect('auth');
		}
    }
	
	public function index()
	{
        $this->load->view('header');
		$this->load->view('side-menu');
		$this->load->view('submenu/submenu-index');
    }

    public function ajax_table_submenu()
    {
        $list = $this->ModelSubMenu->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->nama_sub;
            $row[] = $field->link;
           
			$row[] = "
				<a class='btn btn-sm btn-success' style='color: white;' href='". base_url() . "submenu/edit/" . base64_encode($field->id) ."' title='Edit PO'><span class='fa fa-edit'></span>Edit</a>
				<a class='btn btn-sm btn-danger' style='color: white;' id='hapus' data-id='". $field->id ."' data-toggle='modal' title='Hapus'><span class='fa fa-trash'></span></a>
			";
 
            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->ModelSubMenu->count_all(),
            "recordsFiltered" => $this->ModelSubMenu->count_filtered(),
			"data" => $data,
        );
        //output dalam format JSON
		echo json_encode($output);
    }
    
    public function tambah()
    {
        $data['menu']   = $this->ModelMenu->get();

        $this->load->view('header');
		$this->load->view('side-menu');
		$this->load->view('submenu/submenu-add', $data);
    }

    public function submit() {
        $sub       = $this->ModelSubMenu;
        $validation = $this->form_validation;

        if (isset($_POST['tambah'])) {
            $rules = $sub->rules();
            $validation->set_rules($rules);

            if($validation->run() == false) {
                $this->tambah();
            } else {
                $sub->add();
                $this->session->set_flashdata('berhasil', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Jabatan berhasil ditambahkan</strong></div>');
                redirect('submenu/tambah');
            }
        }
    }

    public function edit($id)
    {
        $id         = base64_decode($id);
        $submenu    = $this->ModelSubMenu;

        $data['submenu'] = $submenu->get_where(['id' => $id])->row(0);
        $data['menu']   = $this->ModelMenu->get();

        //echo json_encode($data['role']->result());
        $this->load->view('header');
		$this->load->view('side-menu');
		$this->load->view('submenu/submenu-edit', $data);
    }

    public function submit_edit() {
        $sub       = $this->ModelSubMenu;
        $validation = $this->form_validation;

        if (isset($_POST['tambah'])) {
            $rules = $sub->rules();
            $validation->set_rules($rules);

            if($validation->run() == false) {
                $id = $this->input->post('id');
                $this->edit(base64_encode($id));
            } else {
                $sub->edit();
                $this->session->set_flashdata('berhasil', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>berhasil ditambahkan</strong></div>');
                redirect('submenu');
            }
        }
    }

    public function hapus()
    {
        $id         = $this->input->post('submenu_id');
        $sub       = $this->ModelSubMenu;

        if ($id != '')
        {
            $sub->id = $id;
            $sub->delete();
        }

        redirect('submenu');
    }

}
