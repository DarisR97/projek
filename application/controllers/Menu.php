<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

    function __construct() {
		parent:: __construct();
        $this->load->model('ModelMenu');
        $this->load->model('ModelMenuRole');
        $this->load->model('ModelRole');

        if($this->session->userdata('is_login') != true)
		{
			redirect('auth');
		}
    }
	
	public function index()
	{
        $this->load->view('header');
		$this->load->view('side-menu');
		$this->load->view('menu/menu-index');
    }

    public function ajax_table_menu()
    {
        $list = $this->ModelMenu->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->menu;
            $row[] = $field->link;
            $row[] = $field->icon;
           
			$row[] = "
				<a class='btn btn-sm btn-success' style='color: white;' href='". base_url() . "menu/edit/" . base64_encode($field->id) ."' title='Edit PO'><span class='fa fa-edit'></span>Edit</a>
				<a class='btn btn-sm btn-danger' style='color: white;' id='hapus' data-id='". $field->id ."' data-toggle='modal' title='Hapus'><span class='fa fa-trash'></span></a>
			";
 
            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->ModelMenu->count_all(),
            "recordsFiltered" => $this->ModelMenu->count_filtered(),
			"data" => $data,
        );
        //output dalam format JSON
		echo json_encode($output);
    }
    
    public function tambah()
    {
        $this->load->view('header');
		$this->load->view('side-menu');
		$this->load->view('menu/menu-add');
    }

    public function submit() {
        $menu       = $this->ModelMenu;
        $validation = $this->form_validation;

        if (isset($_POST['tambah'])) {
            $rules = $menu->rules();
            $validation->set_rules($rules);

            if($validation->run() == false) {
                $this->tambah();
            } else {
                $menu->add();
                $this->session->set_flashdata('berhasil', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Berhasil ditambahkan</strong></div>');
                redirect('menu/tambah');
            }
        }
    }

    public function edit($id)
    {
        $id     = base64_decode($id);
        $menu   = $this->ModelMenu;

        $data['menu'] = $menu->get_where(['id' => $id])->row(0);
        //echo json_encode($data['role']->result());
        $this->load->view('header');
		$this->load->view('side-menu');
		$this->load->view('menu/menu-edit', $data);
    }

    public function submit_edit() {
        $menu       = $this->ModelMenu;
        $validation = $this->form_validation;

        if (isset($_POST['tambah'])) {
            $rules = $menu->rules();
            $validation->set_rules($rules);

            if($validation->run() == false) {
                $id = $this->input->post('id');
                $this->edit(base64_encode($id));
            } else {
                $menu->edit();
                $this->session->set_flashdata('berhasil', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Jabatan berhasil ditambahkan</strong></div>');
                redirect('menu');
            }
        }
    }

    public function hapus()
    {
        $id         = $this->input->post('menu_id');
        $menu       = $this->ModelMenu;

        if ($id != '')
        {
            $menu->id = $id;
            $menu->delete();
        }

        redirect('menu');
    }

    public function role()
    {
        $role_ = $this->ModelRole;
        $data['role'] = $role_->get();
        $this->load->view('header');
		$this->load->view('side-menu');
		$this->load->view('menu/menu-role', $data);
    }

    public function get_rule_menu()
    {
        // if ($_POST)
        // {
            $role_ = $this->ModelRole;
            $menu_ = $this->ModelMenu;
            $menurole_ = $this->ModelMenuRole;

            $role_id = $this->input->post('role');

            $menu = $menu_->get();
            $x = $menu_->get()->result();

            // $data = array();
            echo "<table id='table_role' class='table table-sm table-striped table-bordered table-hover table-full-width dataTable'>
                <thead>
                    <tr>
                        <th width='50px' class='text-center'>NO</th>
                        <th>NAMA MODULE</th>
                        <th>LINK</th>
                        <th width='100px' class='text-center'> HAK AKSES</th>
                    </tr>
                </thead>
                <tbody>";
            $counter = 1;
            foreach($x as $field)
            {
                echo "
                    <tr>
                        <td>" . $counter++ . "</td>
                        <td>" . $field->menu . "</td>
                        <td>asd</td>
                        <td class='text-center'><input type='checkbox' "; 
                        $this->check_module($role_id, $field->id);			
            echo		"onClick='addRule($field->id)'></td>
                    </tr>
                ";
            }
            echo "</tbody></table>";
            //$get_role_menu
        // }
    }

    function check_module($role_id, $menu_id)
    {
        $data 		= array(
                'role_id' => $role_id, 
                'menu_id' => $menu_id );
        $check 		= $this->db->get_where('role_menu_akses', $data);

        if ($check->num_rows() > 0) {
            echo "checked ";
        }
    }
    
    function add_rule()
    {
        $role_id = $_POST['role'];
        $menu_id = $_POST['id_menu'];

        //check if exist
        $data 		= array(
            'role_id' => $role_id, 
            'menu_id' => $menu_id );

        $check 		= $this->db->get_where('role_menu_akses', $data);

        if ($check->num_rows() > 0) {
            $this->db->where($data);
            $this->db->delete('role_menu_akses');
        } else {
            $this->db->insert('role_menu_akses', $data);
        }

    }

}
