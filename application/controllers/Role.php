<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends CI_Controller {

    function __construct() {
		parent:: __construct();
        $this->load->model('ModelRole');

        if($this->session->userdata('is_login') != true)
		{
			redirect('auth');
		}
    }
	
	public function index()
	{
        $this->load->view('header');
		$this->load->view('side-menu');
		$this->load->view('role/role-index');
    }

    public function ajax_table_role()
    {
        $list = $this->ModelRole->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->role;
           
			$row[] = "
				<a class='btn btn-sm btn-success' style='color: white;' href='". base_url() . "role/edit/" . base64_encode($field->id) ."' title='Edit PO'><span class='fa fa-edit'></span>Edit</a>
				<a class='btn btn-sm btn-danger' style='color: white;' id='hapus' data-id='". $field->id ."' data-toggle='modal' title='Hapus'><span class='fa fa-trash'></span></a>
			";
 
            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->ModelRole->count_all(),
            "recordsFiltered" => $this->ModelRole->count_filtered(),
			"data" => $data,
        );
        //output dalam format JSON
		echo json_encode($output);
    }
    
    public function tambah()
    {
        $this->load->view('header');
		$this->load->view('side-menu');
		$this->load->view('role/role-add');
    }

    public function submit() {
        $role       = $this->ModelRole;
        $validation = $this->form_validation;

        if (isset($_POST['tambah'])) {
            $rules = $role->rules();
            $validation->set_rules($rules);

            if($validation->run() == false) {
                $this->tambah();
            } else {
                $role->add();
                $this->session->set_flashdata('berhasil', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Jabatan berhasil ditambahkan</strong></div>');
                redirect('role/tambah');
            }
        }
    }

    public function edit($id)
    {
        $id     = base64_decode($id);
        $role   = $this->ModelRole;

        $data['role'] = $role->get_where(['id' => $id])->row(0);
        //echo json_encode($data['role']->result());
        $this->load->view('header');
		$this->load->view('side-menu');
		$this->load->view('role/role-edit', $data);
    }

    public function submit_edit() {
        $role       = $this->ModelRole;
        $validation = $this->form_validation;

        if (isset($_POST['tambah'])) {
            $rules = $role->rules();
            $validation->set_rules($rules);

            if($validation->run() == false) {
                $id = $this->input->post('id');
                $this->edit(base64_encode($id));
            } else {
                $role->edit();
                $this->session->set_flashdata('berhasil', '<div class="alert alert-success fade-in"><a href="#" class="close" style="text-decoration: none;" data-dismiss="alert" aria-label="close">&times;</a><strong>Jabatan berhasil ditambahkan</strong></div>');
                redirect('role');
            }
        }
    }

    public function hapus()
    {
        $id         = $this->input->post('role_id');
        $role       = $this->ModelRole;

        if ($id != '')
        {
            $role->id = $id;
            $role->delete();
        }

        redirect('role');
    }

    public function upload_file()
    {
       
        // $id = $_POST['id_'];
        // $nama_file = $id . '-' . date('d-m-Y') . '-' . $_FILES['file']['name'];

        // $config['upload_path']          = './assets/';
		// $config['allowed_types']        = 'gif|jpg|png';
		// $config['max_size']             = 100;
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;
 
		// $this->load->library('upload', $config);
 
		// if ( ! $this->upload->do_upload('berkas')){
		// 	$error = array('error' => $this->upload->display_errors());
		// 	//$this->load->view('v_upload', $error);
		// }else{
		// 	$data = array('upload_data' => $this->upload->data());
		// 	//$this->load->view('v_upload_sukses', $data);
        // }
        echo json_encode($_POST);
    }

}
