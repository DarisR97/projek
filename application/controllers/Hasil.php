<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hasil extends CI_Controller {

    function __construct() {
		parent:: __construct();
        $this->load->model('ModelTransaksi');

        if($this->session->userdata('is_login') != true)
		{
			redirect('auth');
		}
    }
	
	
	public function index()
	{
		$this->load->view('header');
		$this->load->view('side-menu');
		$this->load->view('transaksi/transaksi-show');
    }
    
    
    public function ajax_table_transaksi()
    {
        $list = $this->ModelTransaksi->get_datatables();
        $data = array();
        $no = $_POST['start'];

        
        foreach ($list as $field) {
            $status = $this->db->get_where('dana', ['IdPermohonan' => $field->IdPermohonan])->num_rows();
            if($status != 0)
            {
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = $field->CatatanKegiatan;
                $row[] = $field->TglPermohonan;
                $row[] = $field->nama;
                $row[] = "
                    <a class='btn btn-sm btn-primary' style='color: white;' href='". base_url() . "hasil/kirim_berkas/" . base64_encode($field->IdPermohonan) ."' title='Edit PO'><span class='fa fa-check'></span> Kirim Berkas</a>
                ";
    
                $data[] = $row;
            }
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->ModelTransaksi->count_all(),
            "recordsFiltered" => $this->ModelTransaksi->count_filtered(),
			"data" => $data,
        );
        //output dalam format JSON
		echo json_encode($output);
    }

    public function lihat_hasil($id)
    {
        $idPermohonan = base64_decode($id);
        $data['h'] = $this->ModelTransaksi->get_where_join(['IdPermohonan' => $idPermohonan])->row();
        $data['d'] = $this->ModelTransaksi->get_transaksi_detail(['IdPermohonan' => $idPermohonan])->result();
        $data['dana'] = $this->db->get_where('dana', ['IdPermohonan' => $idPermohonan])->row();

        $this->load->view('header');
		$this->load->view('side-menu');
        $this->load->view('transaksi/transaksi-show-detail', $data);
    }

    public function kirim_berkas($id)
    {
        $idPermohonan = base64_decode($id);
        $data['h'] = $this->ModelTransaksi->get_where_join(['IdPermohonan' => $idPermohonan])->row();
        $data['d'] = $this->ModelTransaksi->get_transaksi_detail(['IdPermohonan' => $idPermohonan])->result();
        $data['dana'] = $this->db->get_where('dana', ['IdPermohonan' => $idPermohonan])->row();

        $this->load->view('header');
		$this->load->view('side-menu');
        $this->load->view('transaksi/transaksi-kirim-berkas', $data);
    }    
}
