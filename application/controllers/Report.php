<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

    function __construct() {
		parent:: __construct();
        $this->load->model('ModelReport');

        if($this->session->userdata('is_login') != true)
		{
			redirect('auth');
		}
    }
	
	
	public function index()
	{
		$this->load->view('header');
		$this->load->view('side-menu');
		$this->load->view('report/report');
    }
    
    
    public function ajax_table_report()
    {
        $list = $this->ModelReport->get_datatables();
        $data = array();
        $no = $_POST['start'];

        
        foreach ($list as $field) {
            $status = 'Dalam Proses';
            if ($field->App1 == '1' && $field->App2 == '1' && $field->App3 == '1' && $field->App4 == '1' && $field->App5 == '1')
            {
                $status = "Disetujui";
            }

            if ($field->Reject == 1)
            {
                $status = "Ditolak";
            }

          
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = $field->CatatanKegiatan;
                $row[] = $field->TglPermohonan;
                $row[] = $field->nama;
                $row[] = $field->bagian;
                $row[] = $field->nodpa;
                $row[] = $status;
                // $row[] = "
                //     <a class='btn btn-sm btn-primary' style='color: white;' href='". base_url() . "hasil/kirim_berkas/" . base64_encode($field->IdPermohonan) ."' title='Edit PO'><span class='fa fa-check'></span> Lihat Detail</a>
                // ";
    
                $data[] = $row;
            
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->ModelReport->count_all(),
            "recordsFiltered" => $this->ModelReport->count_filtered(),
			"data" => $data,
        );
        //output dalam format JSON
		echo json_encode($output);
    }

    public function ajax_table_report_cari()
    {
        $list = $this->ModelReport->get_datatables();
        $data = array();
        $no = $_POST['start'];

        
        foreach ($list as $field) {
            $status = 'Dalam Proses';
            if ($field->App1 == '1' && $field->App2 == '1' && $field->App3 == '1' && $field->App4 == '1' && $field->App5 == '1')
            {
                $status = "Disetujui";
            }

            if ($field->Reject == 1)
            {
                $status = "Ditolak";
            }

            if (isset($_POST['aksi']) == '1')
            {
                if ($_POST['aksi'] == 1)
                {
                    //setujui
                    if ($field->App1 == '1' && $field->App2 == '1' && $field->App3 == '1' && $field->App4 == '1' && $field->App5 == '1')
                    {
                        $no++;
                        $row = array();
                        $row[] = 1;
                        $row[] = $field->CatatanKegiatan;
                        $row[] = $field->TglPermohonan;
                        $row[] = $field->nama;
                        $row[] = $field->bagian;
                        $row[] = $field->nodpa;
                        $row[] = $status;
                        // $row[] = "
                        //     <a class='btn btn-sm btn-primary' style='color: white;' href='". base_url() . "hasil/kirim_berkas/" . base64_encode($field->IdPermohonan) ."' title='Edit PO'><span class='fa fa-check'></span> Lihat Detail</a>
                        // ";
                        $data[] = $row;
                    }
                }
            }

            if (isset($_POST['aksi']) == '2')
            {
                if ($_POST['aksi'] == 2)
                {
                    //setujui
                    if (($field->App1 == '0' || $field->App2 == '0' || $field->App3 == '0' || $field->App4 == '0' || $field->App5 == '0') && $field->Reject != 1)
                    {
                        $no++;
                        $row = array();
                        $row[] = 1;
                        $row[] = $field->CatatanKegiatan;
                        $row[] = $field->TglPermohonan;
                        $row[] = $field->nama;
                        $row[] = $field->bagian;
                        $row[] = $field->nodpa;
                        $row[] = $status;
                        // $row[] = "
                        //     <a class='btn btn-sm btn-primary' style='color: white;' href='". base_url() . "hasil/kirim_berkas/" . base64_encode($field->IdPermohonan) ."' title='Edit PO'><span class='fa fa-check'></span> Lihat Detail</a>
                        // ";
                        $data[] = $row;
                    }
                }
            }

            if (isset($_POST['aksi']) == '3')
            {
                if ($_POST['aksi'] == 3)
                {
                    //setujui
                    if ($field->Reject == 1)
                    {
                        $no++;
                        $row = array();
                        $row[] = 1;
                        $row[] = $field->CatatanKegiatan;
                        $row[] = $field->TglPermohonan;
                        $row[] = $field->nama;
                        $row[] = $field->bagian;
                        $row[] = $field->nodpa;
                        $row[] = $status;
                        // $row[] = "
                        //     <a class='btn btn-sm btn-primary' style='color: white;' href='". base_url() . "hasil/kirim_berkas/" . base64_encode($field->IdPermohonan) ."' title='Edit PO'><span class='fa fa-check'></span> Lihat Detail</a>
                        // ";
                        $data[] = $row;
                    }
                }
            }

            if (isset($_POST['aksi']) == '4')
            {
                if ($_POST['aksi'] == 4)
                {
                        $no++;
                        $row = array();
                        $row[] = 1;
                        $row[] = $field->CatatanKegiatan;
                        $row[] = $field->TglPermohonan;
                        $row[] = $field->nama;
                        $row[] = $field->bagian;
                        $row[] = $field->nodpa;
                        $row[] = $status;
                        // $row[] = "
                        //     <a class='btn btn-sm btn-primary' style='color: white;' href='". base_url() . "hasil/kirim_berkas/" . base64_encode($field->IdPermohonan) ."' title='Edit PO'><span class='fa fa-check'></span> Lihat Detail</a>
                        // ";
                        $data[] = $row;
                }
            }
    
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->ModelReport->count_all(),
            "recordsFiltered" => $this->ModelReport->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
		echo json_encode($output);
    }

    public function lihat_hasil($id)
    {
        $idPermohonan = base64_decode($id);
        $data['h'] = $this->ModelTransaksi->get_where_join(['IdPermohonan' => $idPermohonan])->row();
        $data['d'] = $this->ModelTransaksi->get_transaksi_detail(['IdPermohonan' => $idPermohonan])->result();
        $data['dana'] = $this->db->get_where('dana', ['IdPermohonan' => $idPermohonan])->row();

        $this->load->view('header');
		$this->load->view('side-menu');
        $this->load->view('transaksi/transaksi-show-detail', $data);
    }

    public function cari()
    {
        echo json_encode($_POST);
    }

}
