<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dana extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('ModelTransaksi');
    }
	
	public function index()
	{
		$this->load->view('header');
		$this->load->view('side-menu');
		$this->load->view('dana/dana-index');
    }
    
    public function ajax_table_transaksi()
    {
        $list = $this->ModelTransaksi->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->CatatanKegiatan;
            $row[] = $field->TglPermohonan;
            $row[] = $field->nama;
			$row[] = "
                <a class='btn btn-sm btn-primary' style='color: white;' href='". base_url() . "dana/detail/" . base64_encode($field->IdPermohonan) ."' title='Edit PO'><span class='fa fa-check'></span> Lihat Hasil Persetujuan</a>
			";
 
            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->ModelTransaksi->count_all(),
            "recordsFiltered" => $this->ModelTransaksi->count_filtered(),
			"data" => $data,
        );
        //output dalam format JSON
		echo json_encode($output);
    }

    public function detail($id)
    {
        $idPermohonan = base64_decode($id);
        $data['h'] = $this->ModelTransaksi->get_where_join(['IdPermohonan' => $idPermohonan])->row();
        $data['d'] = $this->ModelTransaksi->get_transaksi_detail(['IdPermohonan' => $idPermohonan])->result();

        //cek ada atau tidak
        $status = $this->db->get_where('dana', ['IdPermohonan' => $idPermohonan])->num_rows();
        if ($status == 0) {
            $d = array(
                'IdPermohonan'          => $idPermohonan,
                'berkas'                => 0,
                'tanda_tangan'          => 0,
                'rencana_pelaksanaan'   => 0,
                'no_rekening'           => 0,
                'UserId'                => $this->session->userdata('id_user'),
            );
            $this->db->insert('dana', $d);
        } 
        $data['dana'] = $this->db->get_where('dana', ['IdPermohonan' => $idPermohonan])->row();

        $this->load->view('header');
		$this->load->view('side-menu');
        $this->load->view('dana/dana-setujui', $data);
        // echo json_encode($data['d']);
        // echo json_encode($status);
    }

    public function setujui()
    {
        if ($_POST)
        {
            $id = $_POST['id_permohonan'];
            $cek = $_POST['cek'];

            //cek ada atau tidak
            $status = $this->db->get_where('dana', ['IdPermohonan' => $id])->num_rows();
            
            //simpan jika tida ada
            if ($status == 0) {
                //simpan
                $data = array(
                    'IdPermohonan'          => $id,
                    'berkas'                => 0,
                    'tanda_tangan'          => 0,
                    'rencana_pelaksanaan'   => 0,
                    'no_rekening'           => 0,
                    'UserId'                => $this->session->userdata('id_user'),
                );
                $this->db->insert('dana', $data);
            } 
                //simpan
                if ($cek == 1) {
                    $data = array(
                        'berkas'                => 1,
                    );
                    $this->db->where('IdPermohonan', $id);
                    $this->db->update('dana', $data);
                } else if ($cek == 2) {
                    $data = array(
                        'berkas'                => 0,
                    );
                    $this->db->where('IdPermohonan', $id);
                    $this->db->update('dana', $data);
                } else if ($cek == 3) {
                    $data = array(
                        'tanda_tangan' => 1,
                    );
                    $this->db->where('IdPermohonan', $id);
                    $this->db->update('dana', $data);
                } else if ($cek == 4) {
                    $data = array(
                        'tanda_tangan' => 0,
                    );
                    $this->db->where('IdPermohonan', $id);
                    $this->db->update('dana', $data);
                } else if ($cek == 5) {
                    $data = array(
                        'rencana_pelaksanaan' => 1,
                    );
                    $this->db->where('IdPermohonan', $id);
                    $this->db->update('dana', $data);
                } else if ($cek == 6) {
                    $data = array(
                        'rencana_pelaksanaan' => 0,
                    );
                    $this->db->where('IdPermohonan', $id);
                    $this->db->update('dana', $data);
                } else if ($cek == 7) {
                    $data = array(
                        'no_rekening' => 1,
                    );
                    $this->db->where('IdPermohonan', $id);
                    $this->db->update('dana', $data);
                } else if ($cek == 8) {
                    $data = array(
                        'no_rekening' => 0,
                    );
                    $this->db->where('IdPermohonan', $id);
                    $this->db->update('dana', $data);
                }
            
           
            echo json_encode(['status' => 'success']);
         
        }
    }

    public function upload_file()
    {
        $id = $_POST['id_'];
        $new_name = $id . '-' . date('d-m-Y') . '-' . $_FILES["berkas"]['name'];;
       
        $config['upload_path']          = './assets/dana';
        $config['allowed_types']        = 'gif|jpg|png|pdf|';
        $config['max_size']             = 2048;
        $config['file_name']            = $new_name;
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;
    
        $this->load->library('upload', $config);
    
        if ( ! $this->upload->do_upload('berkas')){
            $error = array('error' => $this->upload->display_errors());
            echo json_encode($error);
            //$this->load->view('v_upload', $error);
        }else{
            $data = array('upload_data' => $this->upload->data());
            $this->db->where('IdPermohonan', $id);
            $this->db->update('trxanggaranh', ['berkas_dana' => $new_name]);
            //$this->load->view('v_upload_sukses', $data);
        }

        redirect('hasil/kirim_berkas/' . base64_encode($id));
    }
    public function download_file($id)
    {
        $file = $this->db->get_where('trxanggaranh', ['IdPermohonan' => $id])->row();
        force_download('assets/dana/' . $file->berkas_dana, NULL);
        // $this->db->where('IdPermohonan', $id);
        // $this->db->update('trxanggaranh', ['berkas_dana' => $new_name]);
        // edirect('dana/detail/' . base64_encode($id));
    }

    public function delete_file($id)
    {
        $file = $this->db->get_where('trxanggaranh', ['IdPermohonan' => $id])->row();
        unlink('assets/dana/' . $file->berkas_dana);

        $this->db->where('IdPermohonan', $id);
        $this->db->update('trxanggaranh', ['berkas_dana' => '']);
        redirect('hasil/kirim_berkas/' . base64_encode($id));
    }
}
